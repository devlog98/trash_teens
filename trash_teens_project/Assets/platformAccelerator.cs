﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class platformAccelerator : MonoBehaviour
{
    public GameObject[] platformsToAccelerate;
    public float accelerationQuantity;

    public void Activate(int activationIndex)
    {
        for(int i = 0; i < platformsToAccelerate.Length; i++)
        {
            platformsToAccelerate[i].GetComponent<MovingPlatform>().routes[platformsToAccelerate[i].GetComponent<MovingPlatform>().curRoute].timeToFinish -= accelerationQuantity;

            platformsToAccelerate[i].GetComponent<MovingPlatform>().looping = true;

            StartCoroutine(LoopOff());
        }
    }

    IEnumerator LoopOff()
    {
        yield return new WaitForSeconds(0.5f);

        for (int i = 0; i < platformsToAccelerate.Length; i++)
        {
            platformsToAccelerate[i].GetComponent<MovingPlatform>().looping = false;
        }   
    }
}
