﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class GamepadMenu : MonoBehaviour
{
    EventSystem es;
    public GameObject[] selectionOrder;
    public enum Direction { Horizontal, Vertical };
    public Direction dir;

    private int index = 0;
    bool canMove = true;
    bool canInvoke = true;

    private void Start()
    {
        es = GameObject.Find("EventSystem").GetComponent<EventSystem>();
        es.SetSelectedGameObject(null);
        es.SetSelectedGameObject(selectionOrder[0]);
    }

    private void Update()
    {
        if(Input.GetButtonDown("Submit") && canInvoke)
        {
            selectionOrder[index].GetComponent<Button>().onClick.Invoke();
            canInvoke = false;
        }

        MoveNext();
    }
    
    void MoveNext()
    {
        if (dir == Direction.Horizontal)
        {
            Debug.Log(index);
            // se voce apertar pra frente
            if ((Input.GetAxisRaw("K_MainHorizontal") > 0 || Input.GetAxisRaw("J_MainHorizontal") > 0))
            {
                if (canMove)
                {
                    canMove = false;
                    // se o index couber no tamanho do array
                    if (index < selectionOrder.Length - 1)
                    {
                        index++;
                        // vai pro botão
                        es.SetSelectedGameObject(selectionOrder[index]);
                    }
                    else
                    {
                        // senão, volta pro começo
                        es.SetSelectedGameObject(selectionOrder[0]);
                        index = 0;
                    }
                }
            }
            else if ((Input.GetAxisRaw("K_MainHorizontal") == 0 || Input.GetAxisRaw("J_MainHorizontal") == 0))
            {
                canMove = true;
            }

            // se voce apertar  pra trás
            if (Input.GetAxisRaw("K_MainHorizontal") < 0 || Input.GetAxisRaw("J_MainHorizontal") < 0)
            {
                if (canMove)
                {
                    canMove = false;
                    // se o index couber no tamanho do array
                    if (index > 0)
                    {
                        index--;
                        // vai pro botão
                        es.SetSelectedGameObject(selectionOrder[index]);
                    }
                    else
                    {
                        // senão, volta pro começo
                        es.SetSelectedGameObject(selectionOrder[selectionOrder.Length - 1]);
                        index = selectionOrder.Length - 1;
                    }
                }
            }
            else if ((Input.GetAxisRaw("K_MainHorizontal") == 0 || Input.GetAxisRaw("J_MainHorizontal") == 0))
            {
                canMove = true;
            }
        }

    }
}

    

