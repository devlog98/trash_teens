﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Bomb : MonoBehaviour {
    public float seconds;
    public float radius;
    public LayerMask destructables;
    public bool canTeleport = true;
    public ParticleSystem explosionParticle;

    [SerializeField] AudioClip explosionClip;

    public void Start()
    {
        StartCoroutine(CountdownCoroutine());
    }

    public IEnumerator CountdownCoroutine()
    {
        while (seconds >= 0)
        {
            seconds -= Time.deltaTime;
            //Debug.Log("Tempo para detonação: " + timer);
            yield return null;
        }

        StopCoroutine("CountdownCoroutine");
        Detonate();        
    }

    public void Detonate()
    {
        Collider[] overlappingColliders = Physics.OverlapSphere(transform.position, radius, destructables);

        foreach (Collider collider in overlappingColliders)
        {
            if (collider.gameObject.tag != "Player")
                Destroy(collider.gameObject);
            else
                collider.gameObject.GetComponent<PlayerModel>().PlayerDeath();
        }

        explosionParticle.transform.position = new Vector3(this.transform.position.x, this.transform.position.y + 3.44f, this.transform.position.z);
        explosionParticle.transform.parent = null;
        explosionParticle.transform.rotation = new Quaternion(0, 0, 0, 0);
        explosionParticle.gameObject.SetActive(true);
        explosionParticle.Play();

        AudioManager.instance.MakeNoise(explosionClip, explosionParticle.gameObject);

        Destroy(this.gameObject, 4.1f);
        this.gameObject.GetComponent<Renderer>().enabled = false;
        this.gameObject.GetComponent<Collider>().enabled = false;
        Destroy(explosionParticle.gameObject, 4f);
        Debug.Log("Detonou");
    }

    public void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, radius);
    }
}
