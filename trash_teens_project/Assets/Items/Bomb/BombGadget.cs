﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombGadget : MonoBehaviour {
    public float seconds;
    public float radius;
    public float force;   
    public Bomb throwable;
    public LayerMask destructables;
    public BombTimer timer;
    public float height;

    private int throwableIndex = 0;

    public void Throw()
    {
        Bomb dynamite = Instantiate(throwable, transform.position + new Vector3(0, 2, 0) + transform.TransformDirection(Vector3.up) * 2, transform.rotation);
        BombTimer dynamiteTimer = Instantiate(timer, dynamite.transform.position, dynamite.transform.rotation);
        Plant(dynamite, dynamiteTimer, throwableIndex);

        Vector3 throwDirection = transform.position - GameObject.FindWithTag("MainCamera").transform.position;
        dynamite.GetComponent<Rigidbody>().AddForce(throwDirection * force, ForceMode.Impulse);
    }

    private void Plant(Bomb dynamite, BombTimer dynamiteTimer, int index)
    {
        dynamite.seconds = seconds;
        dynamite.radius = radius;
        dynamite.destructables = destructables;
        dynamite.name = dynamite.name + "_" + index;

        dynamiteTimer.bomb = dynamite;
        dynamiteTimer.height = height;
        dynamiteTimer.name = dynamiteTimer.name + "_" + index;

        index++;
    }
}
