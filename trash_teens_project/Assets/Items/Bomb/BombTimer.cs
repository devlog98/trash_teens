﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BombTimer : MonoBehaviour
{
    public Bomb bomb;
    public float height;

    private Text timer;
    private Camera mainCamera;

    public void Start()
    {
        timer = GetComponentInChildren<Text>();
        mainCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
    }

    public void Update()
    {
        if (bomb)
        {
            timer.text = "" + Mathf.CeilToInt(bomb.seconds);
            transform.position = new Vector3(bomb.transform.position.x, bomb.transform.position.y + height, bomb.transform.position.z);
            transform.rotation = mainCamera.transform.rotation;
        }
        else
            Destroy(this.gameObject);
    }
}
