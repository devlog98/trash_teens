﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GlueGadget : MonoBehaviour
{
    #region ATTRIBUTES
    [SerializeField]
    private float duration;
    [SerializeField]
    private float cooldown;
    [SerializeField]
    private RectTransform cooldownBar;
    [SerializeField]
    private float force;
    [SerializeField]
    private Glue glue;

    public bool canThrow = true;
    #endregion

    #region METHODS
    public void Throw()
    {
        if (canThrow)
        {
            Glue oldGlue = FindObjectOfType<Glue>();
            if (oldGlue)
                oldGlue.Unglue();

            Glue clone = Instantiate(glue, transform.position + transform.TransformDirection(Vector3.up) * 2, transform.rotation);
            clone.Duration = duration;

            Vector3 throwDirection = transform.position - GameObject.FindWithTag("MainCamera").transform.position;
            clone.GetComponent<Rigidbody>().AddForce(throwDirection * force, ForceMode.Impulse);

            canThrow = false;
            StartCoroutine(CooldownCoroutine());
        }
    }

    public IEnumerator CooldownCoroutine()
    {
        cooldownBar.sizeDelta = new Vector2(100, 100);

        while (cooldownBar.sizeDelta.x >= 0)
        {
            Debug.Log("Tocando");
            cooldownBar.sizeDelta = new Vector2(cooldownBar.sizeDelta.x - cooldown, cooldownBar.sizeDelta.y);
            yield return null;
        }

        cooldownBar.sizeDelta = new Vector2(0, cooldownBar.sizeDelta.y);
        canThrow = true;
        StopCoroutine("CooldownCoroutine");
    }
    #endregion
}
