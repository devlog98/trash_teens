﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IGluable
{
    void React();
}
