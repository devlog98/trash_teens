﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum GlueState { Moving, Fixed }

[RequireComponent(typeof(Rigidbody))]
public class Glue : MonoBehaviour
{
    #region ATTRIBUTES
    private float duration;
    private GameObject gluedAt;
    private GameObject gluedThis;

    private Rigidbody rb;

    private GlueState state;

    public bool canTeleport = true;
    #endregion

    #region PROPERTIES
    public float Duration
    {
        get { return duration; }
        set { duration = value; }
    }

    public GlueState State
    {
        get { return state; }
    }
    #endregion

    #region METHODS
    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        state = GlueState.Moving;
    }

    private void Update()
    {
        Debug.Log("glue cur state: " + state);
    }

    public void OnCollisionEnter(Collision coll)
    {
        if (state == GlueState.Moving)
            GlueAt(coll.gameObject);
        else if (state == GlueState.Fixed)
            GlueThis(coll.gameObject);
    }

    private void GlueAt(GameObject gameObject)
    {
        if (gameObject.tag != "Player")
        {
            gluedAt = gameObject;
            rb.isKinematic = true;
            state = GlueState.Fixed;
        }
    }

    private void GlueThis(GameObject gameObject)
    {
        if (gameObject != gluedAt && gameObject.GetComponent<Rigidbody>())
        {
            gluedThis = gameObject;
            gluedThis.GetComponent<Rigidbody>().isKinematic = true;
            StartCoroutine(DurationCoroutine());
        }
    }

    public void Unglue()
    {
        if (gluedThis)
            gluedThis.GetComponent<Rigidbody>().isKinematic = false;
        StopCoroutine("DurationCoroutine");
        StopCoroutine("CooldownCoroutine");
        Destroy(this.gameObject);
    }

    public IEnumerator DurationCoroutine()
    {
        while (duration >= 0)
        {
            duration -= Time.deltaTime;
            yield return null;
        }

        Unglue();
        StopCoroutine("DurationCoroutine");
    }
    #endregion
}
