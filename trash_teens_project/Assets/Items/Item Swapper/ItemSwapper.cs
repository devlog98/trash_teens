﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemSwapper : MonoBehaviour {

    public int[] inventory;
    public int curItem = 3;
    int curIndex;

    public int unlockProgress;

    public bool bombEquipped;

    public Text curItemText;

    public GameObject bouncePadPrefab;
    private GameObject deployedBouncepad;
    private bool bouncePadDeployed = false;
    public float force;

    public GameObject firstHole;
    public GameObject secondHole;

    private bool isHoleDeployed;
    public bool isSecondHoleDeployed;

    public bool canBomb = true;

    public Transform playerModel;
    public GameObject itemWheel;
    public GameObject pausePanel;


    void Start () {
        //curItem = inventory[curIndex];
    }
	
	// Update is called once per frame
	void Update () {
            UseItem();
    }

    void UseItem()
    {
        if (itemWheel.activeInHierarchy == false && pausePanel.activeInHierarchy == false)
        {
            switch (curItem)
            {
                // Bouncepad
                case 1:
                    if (unlockProgress >= 1)
                    {
                        if (InputManager.UseButton(false) && !this.GetComponent<PlayerModel>().carryScript.IsCarrying)
                        {
                            Debug.Log("Use the bounce, is carrying: " + this.GetComponent<PlayerModel>().carryScript.IsCarrying);
                            bouncePadPrefab.SetActive(true);
                            bouncePadPrefab.transform.position = transform.position + playerModel.transform.TransformDirection(Vector3.forward) * 5;
                            bouncePadPrefab.transform.rotation = new Quaternion(0, 0, 0, 0);
                            bouncePadPrefab.GetComponent<Rigidbody>().velocity = Vector3.zero;
                            bouncePadPrefab.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
                            bouncePadDeployed = true;

                            Vector3 throwDirection = transform.position - GameObject.FindWithTag("MainCamera").transform.position;
                            bouncePadPrefab.GetComponent<Rigidbody>().AddForce(throwDirection * force, ForceMode.Impulse);
                        }

                        if (bouncePadDeployed && InputManager.InteractButton(false))
                        {
                            deployedBouncepad.SetActive(false);
                            bouncePadDeployed = false;
                        }
                    }

                    break;

                // Hole
                case 2:

                    if (unlockProgress >= 2)
                    {
                        if (InputManager.UseButton(false) && this.GetComponent<PlayerModel>().isGrounded())
                        {
                            firstHole.transform.position = transform.position + playerModel.transform.TransformDirection(Vector3.forward) * 5;
                            firstHole.SetActive(true);
                            isHoleDeployed = true;
                        }
                        else if (InputManager.InteractButton(false) && this.GetComponent<PlayerModel>().isGrounded())
                        {
                            secondHole.transform.position = transform.position + playerModel.transform.TransformDirection(Vector3.forward) * 5;
                            secondHole.SetActive(true);
                            isSecondHoleDeployed = true;
                        }
                    }

                    break;

                // Bomb
                case 3:

                    if (bombEquipped)
                    {
                        if (InputManager.UseButton(false))
                        {
                            GetComponent<BombGadget>().Throw();
                        }
                    }

                    break;

                case 4:
                    if (unlockProgress >= 4)
                    {
                        if (InputManager.UseButton(false))
                        {
                            GetComponent<PlayerModel>().isGlued = false;
                            GetComponent<GlueGadget>().Throw();
                        }
                    }

                    break;
            }
        }
    }
}
