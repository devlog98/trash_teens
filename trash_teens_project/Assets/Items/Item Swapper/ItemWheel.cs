﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemWheel : MonoBehaviour {

    public Canvas itemCanvas;
    public GameObject pauseCanvas;
    public Button bounceBtn, bombBtn, holeBtn, glueBtn;

    // Update is called once per frame
    void Update () {

        if (InputManager.NextItem())
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            itemCanvas.gameObject.SetActive(true);
            Time.timeScale = 0.2f;

            if(GetComponent<ItemSwapper>().unlockProgress >= 1)
                bounceBtn.gameObject.SetActive(true);
            else
                bounceBtn.gameObject.SetActive(false);


            if (GetComponent<ItemSwapper>().unlockProgress >= 2)
                holeBtn.gameObject.SetActive(true);
            else
                holeBtn.gameObject.SetActive(false);


            if (GetComponent<ItemSwapper>().unlockProgress >= 3)
                bombBtn.gameObject.SetActive(true);
            else
                bombBtn.gameObject.SetActive(false);


            if (GetComponent<ItemSwapper>().unlockProgress >= 4)
                glueBtn.gameObject.SetActive(true);
            else
                glueBtn.gameObject.SetActive(false);
        }

        else if(pauseCanvas.activeInHierarchy == false)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            itemCanvas.gameObject.SetActive(false);
            Time.timeScale = 1f;
        }
	}

    public void SwapItem(int item)
    {
        GetComponent<ItemSwapper>().curItem = item;
    }
}
