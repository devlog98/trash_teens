﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoleGadget : MonoBehaviour
{
    public GameObject firstHole;
    public GameObject secondHole;

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player" &&
            other.gameObject.GetComponent<PlayerModel>().canTeleport == true
            && other.gameObject.GetComponent<ItemSwapper>().isSecondHoleDeployed == true)
        {
            other.transform.position = new Vector3(secondHole.transform.position.x, secondHole.transform.position.y + 1f, secondHole.transform.position.z);
            other.gameObject.GetComponent<PlayerModel>().canTeleport = false;
        }

        if(other.gameObject.tag == "Bomb" && other.gameObject.GetComponent<Bomb>().canTeleport == true)
        {
            other.transform.position = new Vector3(secondHole.transform.position.x, secondHole.transform.position.y + 1f, secondHole.transform.position.z);
            other.gameObject.GetComponent<Bomb>().canTeleport = false;
        }

        if (other.gameObject.tag == "Glue" && other.gameObject.GetComponent<Glue>().canTeleport == true)
        {
            other.transform.position = new Vector3(secondHole.transform.position.x, secondHole.transform.position.y + 1f, secondHole.transform.position.z);
            other.gameObject.GetComponent<Glue>().canTeleport = false;
        }
    }
}
