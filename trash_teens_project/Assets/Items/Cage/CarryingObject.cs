﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarryingObject : MonoBehaviour {
    [SerializeField] PlayerModel player;
    [SerializeField] BoxCollider throwCollider;

    GameObject carryObject;    
    GameObject carryModel;
    Collider carryCol;
    bool isCarrying;    

    public GameObject CarryObject { get { return carryObject; } }
    public bool IsCarrying { get { return isCarrying; } }
    public Collider CarryCol { get { return carryCol; } }

    private void OnTriggerStay(Collider other) {
        if (other.gameObject.tag == "Carryable") {
            carryCol = other;
        }
    }

    private void OnTriggerExit(Collider other) {
        if (other.gameObject.tag == "Carryable") {
            carryCol = null;
        }
    }

    public void Carry(Collider col) {      
        carryObject = col.gameObject;
        carryModel = carryObject.transform.Find("Model").gameObject;
        carryModel.SetActive(false);

        if (carryObject.transform.parent != this.transform) {
            carryObject.transform.SetParent(this.transform, true);
        }

        throwCollider.enabled = true;
        carryObject.GetComponent<Rigidbody>().isKinematic = true;
        carryObject.GetComponent<BoxCollider>().enabled = false;
        carryObject.transform.position = player.carryPosition.transform.position;

        isCarrying = true;
        player.moveSettings.throwTarget.enabled = true;
    }

    public void Drop(Vector3 dropPlace) {
        carryObject.transform.position = dropPlace;
        carryObject.transform.localRotation = Quaternion.Euler(90, 180, 0);
        carryModel.SetActive(true);

        carryObject.transform.SetParent(null, true);

        throwCollider.enabled = false;
        carryObject.gameObject.GetComponent<BoxCollider>().enabled = true;
        carryObject.GetComponent<Rigidbody>().isKinematic = false; 

        isCarrying = false;
        player.moveSettings.curMoveSpeed = player.moveSettings.defaultMoveSpeed;
        player.moveSettings.jumpForce = player.moveSettings.defaultJumpForce;
        player.moveSettings.throwTarget.enabled = false;
        
        carryCol = null;
    }
}
