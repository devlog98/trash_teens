﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bouncing : MonoBehaviour {

    public float bounceForce = 10f;
    public float objectBounceForce = 2f;
    public Animator anim;

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            anim.SetTrigger("jiggle");
            collision.gameObject.GetComponent<PlayerModel>().moveSettings.jumpCounter = 1;
            collision.gameObject.GetComponent<PlayerModel>().moveSettings.verticalVelocity = bounceForce;
            
        }

        /*if(collision.gameObject.layer == 10)
        {
            Debug.Log("The box hit us");
            if (collision.gameObject.GetComponent<BoxMovement>().isBeingCarried == false)
            collision.gameObject.GetComponent<BoxMovement>().verticalVelocity = objectBounceForce;
        }*/

        if(collision.gameObject.tag == "Bomb" || collision.gameObject.tag == "Glue" || collision.gameObject.tag == "Carryable")
        {
            collision.gameObject.GetComponent<Rigidbody>().AddForce(new Vector3(0, objectBounceForce, 0));
        }
    }
}
