﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxMovement : MonoBehaviour {

    public float verticalVelocity;
    public float gravity;
    public float bounceForce = 2f;
    public BoxCollider col;
    public LayerMask groundLayers;
    public LayerMask bounceLayer;
    public bool isBeingCarried;
    public bool isInsideEnemyArea = true;
	
	void Update () {

		if(!isGrounded() && !isBeingCarried && verticalVelocity >= -0.8f)
        {
            //Debug.Log("Keep Falling");
            verticalVelocity -= gravity;
        }
        else if(isBeingCarried || isGrounded())
        {
            //Debug.Log("Stop Falling");
            verticalVelocity = 0;
        }

        if (!isBeingCarried && Physics.Raycast(transform.position, Vector3.down, 1f, bounceLayer))
        {
            verticalVelocity = bounceForce;
        }

        //GetComponent<CharacterController>().Move(new Vector3(0, verticalVelocity, 0));
        //GetComponent<Rigidbody>().AddForce(0, verticalVelocity, 0);
        transform.Translate(new Vector3(0, verticalVelocity, 0));
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Enemy")
            isInsideEnemyArea = true;
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Enemy")
            isInsideEnemyArea = true;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Enemy")
            isInsideEnemyArea = false;
    }

    public bool isGrounded()
    {
        /*  // Declaring 4 Rays for the corners of the player body
          Vector3 frontRayStart;
          Vector3 backRayStart;
          Vector3 rightRayStart;
          Vector3 leftRayStart;

          // Putting them at the default center position
          frontRayStart = col.bounds.center;
          backRayStart = col.bounds.center;
          rightRayStart = col.bounds.center;
          leftRayStart = col.bounds.center;

          frontRayStart.x += col.bounds.extents.x - 0.4f;
          backRayStart.x -= col.bounds.extents.x - 0.4f;
          rightRayStart.z += col.bounds.extents.z - 0.4f;
          leftRayStart.x -= col.bounds.extents.z - 0.4f;

          Debug.DrawRay(frontRayStart, Vector3.down, Color.red);
          Debug.DrawRay(backRayStart, Vector3.down, Color.red);
          Debug.DrawRay(rightRayStart, Vector3.down, Color.red);
          Debug.DrawRay(leftRayStart, Vector3.down, Color.red);

          if (Physics.Raycast(frontRayStart, Vector3.down, (col.bounds.extents.y / 2) + 0.65f, groundLayers))
              return true;
          else if (Physics.Raycast(backRayStart, Vector3.down, (col.bounds.extents.y / 2) + 0.65f, groundLayers))
              return true;
          else if (Physics.Raycast(rightRayStart, Vector3.down, (col.bounds.extents.y / 2) + 0.65f, groundLayers))
              return true;
          else if (Physics.Raycast(leftRayStart, Vector3.down, (col.bounds.extents.y / 2) + 0.65f, groundLayers))
              return true;
          else
              return false;*/
        if (Physics.Raycast(transform.position, Vector3.down, 0.65f, groundLayers))
            return true;
        else
            return false;

    }
}
