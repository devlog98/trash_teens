﻿Shader "Unlit/ocean_shader"
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
		_MyColor("Color", Color) = (1,1,1,1)
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog

			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
				float intensity : PSIZE;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			float4 _MyColor;

			//para cada vértice que tem no objeto
			v2f vert(appdata v)
			{
				v2f o;

				/*float intensity = sin(_Time.y * 0.5 + v.vertex.x * 0.01 + v.vertex.z * 0.01);*/
				float intensity = sin(_Time.y + v.vertex.x + v.vertex.z);
				float waveX = sin(_Time.y + v.vertex.z + v.vertex.y + v.vertex.x) * 0.025;
				float waveY = sin(_Time.y + v.vertex.z + v.vertex.y + v.vertex.x) * 0.025;
				float waveZ = sin(_Time.y + v.vertex.z + v.vertex.y + v.vertex.x) * 0.005;
				float4 myvertex = float4(v.vertex.x + waveX, v.vertex.y + waveY, v.vertex.z + waveZ, v.vertex.w);

				o.vertex = UnityObjectToClipPos(myvertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);

				o.intensity = intensity * 0.1;

				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}

			//para cada pixel que tem na tela
			fixed4 frag(v2f i) : SV_Target
			{
				// sample the texture
				float2 myuv = float2(-i.uv.x, i.uv.y + _Time.x);
				float2 myuv2 = float2(i.uv.x - _Time.x, i.uv.y);

				fixed4 col = tex2D(_MainTex, myuv);
				fixed4 col2 = tex2D(_MainTex, myuv2);
				fixed4 finalCol = ((col + col2) * 0.5) * _MyColor + i.intensity;

				// apply fog
				UNITY_APPLY_FOG(i.fogCoord, finalCol);
				return finalCol;
			}
			ENDCG
		}
	}
}