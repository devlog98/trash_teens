﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour, IActivatable {


    /* -------- HOW TO USE ----------
     * when you want a moving platform, put the model in the scene, and then parent it to an empty gameObject with a 1,1,1 Scale
     * This script goes in this empty game object, NOT IN THE MODEL!!
     * Reason being, you can then scale the model as you wish, and the parented object will maintain the 1,1,1 Scale
     * 
     * If you want it to loop, check looping and have two routes:
     * One going forward
     * And one going backwards
     */


    [System.Serializable]
    public class Routes {
        public Transform startPoint; // Point where the platform will start
        public Transform endPoint; // Point where the platform will end
        public float timeToFinish; // time to cross between both points
        public float timeToWait; // time to wait before moving to the next
    }

    public Routes[] routes; //Points that the object has to travel 

    public int curRoute;
    float elapsedTime = 0;
    public float ElapsedTime { set { elapsedTime = value; } }
    float elapsedWait = 0;

    public bool active;
    ActivatablePropertie activatableProperty;

    public bool looping;

    public bool isStoppable = false;

    public bool lockPlayer = false;
    private GameObject player; // variable necessary to stop the player from moving

    public bool isToggle = false;

    void Start() {
        activatableProperty = GetComponent<ActivatablePropertie>();
        player = GameObject.Find("Player");
        curRoute = 0;
        elapsedTime = 0;
        elapsedWait = 0;
    }

    void Update() {
        if (activatableProperty.activated) {
            MoveToEndPoint();
        }
        else if (!activatableProperty.activated && isToggle) {
            GoBack();
        }
    }

    public void Activate(float activationParameter) {
        if (isStoppable)
            activatableProperty.activated = !activatableProperty.activated;
        else
            activatableProperty.activated = true;
    }

    public void Deactivate() {
        curRoute = 0;
        activatableProperty.activated = false;
    }

    private void GoBack() {
        if (curRoute - 1 >= 0) {
            if (elapsedTime <= routes[curRoute - 1].timeToFinish) {
                this.transform.position = Vector3.Lerp(routes[curRoute - 1].endPoint.position, routes[curRoute - 1].startPoint.position, (elapsedTime / routes[curRoute - 1].timeToFinish));

                elapsedTime += Time.deltaTime;

                if (lockPlayer) {
                    player.GetComponent<PlayerModel>().LockPlayer(lockRigidbody: false);
                }
            }
            else if (elapsedWait >= routes[curRoute - 1].timeToWait) {
                if (curRoute - 1 >= 0) {
                    curRoute--;
                    elapsedTime = 0;
                    elapsedWait = 0;
                }
                else {
                    if (player != null) {
                        player.GetComponent<PlayerModel>().UnlockPlayer(unlockRigidbody: false);
                    }
                }
            }
            else {
                elapsedWait += Time.deltaTime;
            }
        }
    }

    private void MoveToEndPoint() {
        if (curRoute < routes.Length) {
            if (elapsedTime <= routes[curRoute].timeToFinish) {
                this.transform.position = Vector3.Lerp(routes[curRoute].startPoint.position, routes[curRoute].endPoint.position, (elapsedTime / routes[curRoute].timeToFinish));

                elapsedTime += Time.deltaTime;

                if (lockPlayer) {
                    player.GetComponent<PlayerModel>().LockPlayer(lockRigidbody: false);
                }
            }
            else if (elapsedWait >= routes[curRoute].timeToWait) {               
                if (curRoute < routes.Length) {
                    curRoute++;
                    elapsedTime = 0;
                    elapsedWait = 0;
                }
                else {
                    if (player != null) {
                        player.GetComponent<PlayerModel>().UnlockPlayer(unlockRigidbody: false);
                    }
                }
            }
            else {
                elapsedWait += Time.deltaTime;
            }
        }
        else if (looping) {
            curRoute = 0;
            elapsedTime = 0;
            elapsedWait = 0;
        }
    }
}