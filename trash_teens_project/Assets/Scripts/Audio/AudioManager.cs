﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class AudioManager : MonoBehaviour {
    public static AudioManager instance; //static instance can be called any time   
    AudioSource sourceFX;

    //event permits observational pattern
    public static event Action<GameObject> MakeNoiseEvent = delegate { };    

    void Awake() {
        if (instance != null && instance != this) {
            Destroy(this.gameObject);
        }
        else {
            instance = this;
        }
    }

    void Start() {
        sourceFX = GetComponent<AudioSource>();
    }

    //UI sounds
    public void PlayClip(AudioClip clip) {
        sourceFX.PlayOneShot(clip);
    }

    //ambient sounds
    public void MakeNoise(AudioClip clip, GameObject go) {
        AudioSource.PlayClipAtPoint(clip, go.transform.position);
        MakeNoiseEvent(go);
    }
}