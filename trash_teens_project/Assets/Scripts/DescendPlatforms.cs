﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DescendPlatforms : MonoBehaviour, IActivatable {
    public bool isActive;
    public float distanceToDescend = 70;
    public float timeToDescend;

    [Header("Options for Enemy Trap")] //if you want Ignacio to return patrolling, choose at least one
    [SerializeField] bool unlockAtStartPosition = true;
    [SerializeField] bool unlockAtEndPosition;

    float elapsedTime;
    Vector3 startPos, endPos, targetPos;
    public ActivatablePropertie activatableProperty;


    void Start() {
        startPos = this.transform.position;
        endPos = new Vector3(transform.position.x, startPos.y - distanceToDescend, transform.position.z);
        activatableProperty = this.GetComponent<ActivatablePropertie>();
    }

    void Update() {
        if (activatableProperty.activated) {
            if (transform.childCount > 0) {
                BroadcastMessage("Lock", true);
            }

            if (this.transform.position != endPos) {
                this.transform.position = Vector3.Lerp(this.transform.position, endPos, elapsedTime / timeToDescend);
                elapsedTime += Time.deltaTime;
                return;
            }

            UnlockChildren();
            elapsedTime = 0;
        }
        else
        {
            if (this.transform.position != startPos)
            {
                this.transform.position = Vector3.Lerp(this.transform.position, startPos, elapsedTime / timeToDescend);
                elapsedTime += Time.deltaTime;
                return;
            }
            elapsedTime = 0;
        }
        /*else {
            if (Vector3.Distance(this.transform.position, startPos) <= 1f) {
                targetPos = endPos;
            } else {
                targetPos = startPos;
            }
        }*/
    }

    public void Activate(float activationParameter) {
        elapsedTime = 0;
        activatableProperty.activated = !activatableProperty.activated;
    }

    public void UnlockChildren() {
        if ((targetPos == startPos && unlockAtStartPosition) || (targetPos == endPos && unlockAtEndPosition)) {
            Debug.Log("Unlock Broadcast");
            BroadcastMessage("Lock", false);
        }
    }
}
