﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pause : MonoBehaviour {


    public GameObject pausePanel;
	
	// Update is called once per frame
	void Update () {
		
        if(Input.GetButtonDown("Cancel") && pausePanel.activeInHierarchy == false)
        {
            // Enabling Mouse
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;

            pausePanel.SetActive(true);
            Time.timeScale = 0f;
        }
        else if(Input.GetButtonDown("Cancel") && pausePanel.activeInHierarchy == true)
        {
            // Disabling Mouse
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;

            pausePanel.SetActive(false);
            Time.timeScale = 1.2f;
        }
	}
}
