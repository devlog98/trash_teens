﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager
{
    #region CHARACTER CONTROL
    //AXIS
    public static float HorizontalInput()
    {
        float r = 0.0f;

        r += Input.GetAxis("K_MainHorizontal");
        r += Input.GetAxis("J_MainHorizontal");

        return Mathf.Clamp(r, -1.0f, 1.0f);
    }

    public static float VerticalInput()
    {
        float r = 0.0f;

        r += Input.GetAxis("K_MainVertical");
        r += Input.GetAxis("J_MainVertical");

        return Mathf.Clamp(r, -1.0f, 1.0f);
    }

    //ACTIONS
    public static bool JumpButton()
    { return Input.GetButtonDown("Jump"); }

    public static bool SprintButton()
    { return Input.GetButton("Sprint"); }

    public static bool SneakButton()
    { return Input.GetButton("Sneak"); }

    public static bool CarryButton()
    { return Input.GetButtonDown("Carry"); }

    public static bool DropButton()
    { return Input.GetButtonDown("Drop"); }

    //INVENTORY
    public static bool UseButton(bool hold)
    {
        if (hold)
            return Input.GetButton("UseItem");
        else
            return Input.GetButtonDown("UseItem");
    }

    public static bool InteractButton(bool hold)
    {
        if (hold)
            return Input.GetButton("UseItemSecondary");
        else
            return Input.GetButtonDown("UseItemSecondary");
    }

    public static bool PreviousItem()
    { return Input.GetButtonDown("PreviousItem"); }

    public static bool NextItem()
    { return Input.GetButton("NextItem"); }

    // LEMBRAR DE PERGUNTAR PRO PEDRO
    public static Vector3 MainMovement()
    { return new Vector3(HorizontalInput(), 0, VerticalInput()); }
    #endregion

    #region CAMERA CONTROL
    public static float HorizontalCamera()
    {
        float r = 0.0f;

        r += Input.GetAxis("J_CameraHorizontal");
        r += Input.GetAxis("K_CameraHorizontal");

        return Mathf.Clamp(r, -1.0f, 1.0f);
    }

    public static float VerticalCamera()
    {
        float r = 0.0f;

        r += Input.GetAxis("J_CameraVertical");
        r += Input.GetAxis("K_CameraVertical");

        return Mathf.Clamp(r, -1.0f, 1.0f);
    }
    #endregion
}