﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class PlayerEnteredCheckpoint : MonoBehaviour {

    //public PlayerRespawn playerRespawnCode;

    private RespawnManager_v2 respawnCode;
    public GameObject respawnManager;

    public Vector3 spawnPointVector;
    public Vector3 cageSpawnVector;

    public Transform playerSpawn;
    public Transform cageSpawnPoint;

    private void Start()
    {
        respawnCode = respawnManager.GetComponent<RespawnManager_v2>();

        spawnPointVector = playerSpawn.transform.position;
        cageSpawnVector = cageSpawnPoint.transform.position;

        respawnCode.SavePlayerState(spawnPointVector);
        respawnCode.SavePlayerState(cageSpawnVector);
        respawnCode.SaveObjectState();
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.U))
        {
            Debug.Log("yes hlelo");
            respawnCode.ResetObjectState();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            Debug.Log("Set Spawn point to here");
            CheckpointData.checkpointControl.playerPos = playerSpawn.position;
            respawnCode.SavePlayerState(spawnPointVector);
            respawnCode.SaveObjectState();
        }

        if(other.gameObject.tag == "Carryable" && other.gameObject.GetComponent<Rigidbody>().isKinematic)
        {
            CheckpointData.checkpointControl.cagePos = cageSpawnPoint.position; 
            respawnCode.SaveCageState(cageSpawnVector);
        }
    }

    private void OnTriggerExit(Collider other)
    {
    }
}
