﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerRespawn : MonoBehaviour {


    public Transform currSpawnPoint;
    public GameObject player;
    public float respawnTime;

    public Image blackRespawnScreen;
    private bool isFadingToBlack;
    private bool isFadingToWhite;
    public float fadeSpeed;
    public float fadeLength;

    public PlayerModel playerMoveScript;

    public bool callRespawn = false;

    private bool isRespawning = false;

    // Update is called once per frame
    void Update()
    {
        if(callRespawn)
        {
            Respawn();
        }

        if (isFadingToBlack)
        {
            blackRespawnScreen.color = new Color(blackRespawnScreen.color.r, blackRespawnScreen.color.g, blackRespawnScreen.color.b, Mathf.MoveTowards(blackRespawnScreen.color.a, 1f, fadeSpeed * Time.deltaTime));
            if (blackRespawnScreen.color.a == 1f)
            {
                isFadingToBlack = false;
            }
        }

        if (isFadingToWhite)
        {
            blackRespawnScreen.color = new Color(blackRespawnScreen.color.r, blackRespawnScreen.color.g, blackRespawnScreen.color.b, Mathf.MoveTowards(blackRespawnScreen.color.a, 0f, fadeSpeed * Time.deltaTime));
            if (blackRespawnScreen.color.a == 0f)
            {
                isFadingToWhite = false;
            }
        }
    }

    public void SetSpawnPoint(Transform newPoint)
    {
        currSpawnPoint = newPoint;
    }

    public void Respawn()
    {
        if (!isRespawning && callRespawn)
        {
            callRespawn = false;
            StartCoroutine("RespawnCooldown");
        }
    }

    public IEnumerator RespawnCooldown()
    {

        Debug.Log("Respawn Routine");
        isRespawning = true;

        isFadingToBlack = true;


        yield return new WaitForSeconds(0.5f);

        Debug.Log("We reached this line");
        player.gameObject.SetActive(false);

        yield return new WaitForSeconds(respawnTime);

        isRespawning = false;

        player.transform.position = currSpawnPoint.position + new Vector3(5,0,5);
        player.gameObject.SetActive(true);

        Debug.Log("Player pos: " + player.transform.position);

        yield return new WaitForSeconds(fadeLength);

        isFadingToWhite = true;

    }
}
