﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MSuits.Checkpoint;

public enum PlayerState { Idle, Sneak, Walk, Run, Jump }

[RequireComponent(typeof(Rigidbody))]
public class PlayerModel : MonoBehaviour {
    #region ATTRIBUTES
    //MOVEMENT
    public Rigidbody rb;
    public Transform playerModel;
    public Transform playerPivot;
    public MoveSettings moveSettings;
    public DialogueManager dialogueScript;

    public bool isLocked = false;

    //CARRYING
    public CarryingObject carryScript;
    public GameObject carryHitbox;
    public GameObject carryPosition;
    public GameObject carryDrop;
    private Collider carryCol;
    Vector3 lastTargetPosition;

    //INPUTS
    float horizontalInput, verticalInput;
    bool jumpInput, interactInput, pauseInput;

    public bool IsMoving { get { return horizontalInput != 0 || verticalInput != 0; } }
    public bool InteractInput { get { return interactInput; } }

    //ITEM RELATED
    public bool canTeleport = true;
    public LayerMask teleporterMask;
    public bool isGlued;

    //GENERAL
    public GameObject panel;
    public GameObject gm;
    public GameObject respawnManager;
    bool playDeath = true;
    float lastPos;

    bool throwing; //for throw cage
    public bool Throwing { get { return throwing; } }
    bool sliding; //for the slide trap
    public bool Sliding { get { return sliding; } }

    //DEBUG
    [SerializeField] bool debugThrow;

    //SOUNDS
    [SerializeField] AudioClip stepSound;
    #endregion

    // PARTICLE SYSTEMS
    [SerializeField] ParticleSystem stepParticlesLeft;
    [SerializeField] ParticleSystem stepParticlesRight;
    [SerializeField] ParticleSystem sweat;
    [SerializeField] ParticleSystem smoke;
    private bool playSmokes;

    bool dropButtonEnabled = true;
    public bool DropButtonEnabled { get { return dropButtonEnabled; } set { dropButtonEnabled = value; } }

    #region METHODS
    void Start() {
        rb = GetComponent<Rigidbody>();
        isLocked = false;
    }

    void Update() {
        if (!isLocked) {
            horizontalInput = sliding ? 0 : InputManager.HorizontalInput();
            verticalInput = sliding ? 1 : InputManager.VerticalInput();

            Debug.DrawLine(playerModel.transform.position, playerModel.transform.position + playerModel.transform.forward * 2.5f, Color.red);

            if (sliding) {
                RaycastHit hit;
                if (Physics.Raycast(playerModel.transform.position, playerModel.transform.forward, out hit, 2.5f)) {
                    if (hit.collider.GetType() == typeof(MeshCollider))
                        sliding = false;
                }
            }

            Jump();

            if (InputManager.SprintButton()) {
                Sprint();
            }
            else if (InputManager.SneakButton()) {
                Sneak();
            }
            else if (IsMoving && !sliding) {
                Walk();
            }
            else {
                Idle();
            }

            if (carryScript.IsCarrying) {
                moveSettings.jumpCounter = 2;

                if (InputManager.UseButton(false)) {
                    Throw();
                }
            }
            if (InputManager.InteractButton(false) && carryScript.CarryCol != null) {
                if (!carryScript.IsCarrying) {
                    carryScript.Carry(carryScript.CarryCol);
                    carryCol = carryScript.CarryCol;
                }
                else if (carryScript.IsCarrying && dropButtonEnabled) {
                    Drop(carryDrop.transform.position);
                }
            }

            if (canTeleport == false) {
                if (!TeleportChecker())
                    canTeleport = true;
            }

            //If you're grounded save your Y Position so you can die
            if (isGrounded()) {
                lastPos = this.transform.position.y;
            }
            // If you fall, drop the box and DIE
            if (transform.position.y <= lastPos - 50) {
                if (playDeath) {
                    StartCoroutine(PlayerDeath());
                    playDeath = false;
                }

                if (carryHitbox.GetComponent<CarryingObject>().IsCarrying)
                    Drop(carryDrop.transform.position);
            }

            // But deactivate the death sequence after you fall a bit more
            if (transform.position.y <= -65)
                gm.GetComponent<PlayerRespawn>().callRespawn = false;

            // Play Smoke Particles after jumping
            if (playSmokes) {
                if (isGrounded()) {
                    // play smoke particle
                    //var em = smoke.emission;
                    //em.enabled = true;

                    Debug.Log("wow we shouldnt be grounded");
                    smoke.Play();

                    // Deactivate them
                    playSmokes = false;
                }
            }

            //throw arc debug
            if (debugThrow) {
                DrawPath();
            }
        }
    }

    void FixedUpdate() {
        if (!isLocked)
            Move();
    }

    void Move() {
        moveSettings.x = horizontalInput;
        moveSettings.z = verticalInput;

        Vector3 forwardVector;
        if (!sliding) {
            forwardVector = (playerPivot.transform.forward * moveSettings.z) + (playerPivot.transform.right * moveSettings.x);
        }
        else {
            forwardVector = (playerModel.forward * moveSettings.z) + (playerModel.right * moveSettings.x);
        }

        moveSettings.movement = forwardVector;

        //rotating character
        if ((moveSettings.x != 0 || moveSettings.z != 0) && !sliding) {
            Quaternion newRotation = Quaternion.LookRotation(new Vector3(moveSettings.movement.x, 0f, moveSettings.movement.z));
            playerModel.transform.rotation = Quaternion.Slerp(playerModel.transform.rotation, newRotation, moveSettings.rotateSpeed * Time.deltaTime);
        }

        //applying forces
        moveSettings.movement = moveSettings.movement.normalized * moveSettings.curMoveSpeed;
        moveSettings.movement.y = moveSettings.verticalVelocity;

        rb.AddForce(moveSettings.movement, ForceMode.VelocityChange); // Still add a 

        rb.velocity = new Vector3(rb.velocity.x, moveSettings.movement.y, rb.velocity.z);
    }

    void Jump() {
        if (isGrounded()) // If you are on the ground
        {
            moveSettings.jumpCounter = 1; // Reset the jump counter

            if (moveSettings.verticalVelocity <= 0)
                moveSettings.verticalVelocity = 0;

            if (InputManager.JumpButton()) // When you jump
            {
                moveSettings.verticalVelocity = moveSettings.jumpForce; // Raise the vertical velocity to be equal to JumpForce (Jump)
            }
        }
        else if (moveSettings.jumpCounter < moveSettings.jumpMaxCounter && InputManager.JumpButton()) // If a jump is still possible (If you jumped less than 2 times) & you press Space
        {
            moveSettings.curMoveSpeed = moveSettings.defaultMoveSpeed; // Returning the move speed to normal (in case we were bursting)
            moveSettings.verticalVelocity = moveSettings.jumpForce; // Jump Again
            moveSettings.jumpCounter++; // Raise the counter again

        }
        else if (moveSettings.verticalVelocity >= moveSettings.maxVerticalVelocity && !isGlued) // Needs a maximum vertical velocity to prevent getting too crazy
        {
            playSmokes = true;
            moveSettings.verticalVelocity -= moveSettings.gravity * Time.deltaTime; // Reduce Vertical Velocity till you hit the ground
        }
    }

    public void Idle() {
        moveSettings.state = PlayerState.Idle;
    }

    public void Walk() {
        moveSettings.state = PlayerState.Walk;

        if (!carryScript.IsCarrying)
            moveSettings.curMoveSpeed = moveSettings.defaultMoveSpeed;
        else
            moveSettings.curMoveSpeed = moveSettings.carrySpeed;

        rb.drag = moveSettings.defaultDrag;

        var em = sweat.emission; // Sweat particle off
        em.enabled = false;
    }

    public void Sneak() {
        if (isGrounded()) {
            moveSettings.state = PlayerState.Sneak;

            if (!carryScript.IsCarrying) {
                moveSettings.curMoveSpeed = moveSettings.defaultMoveSpeed - moveSettings.sneakAmount;
            }
            else if (carryScript.IsCarrying) {
                moveSettings.curMoveSpeed = moveSettings.carrySpeed - moveSettings.sneakAmount;
            }

            rb.drag = moveSettings.defaultDrag;
        }
    }

    public void Sprint() {
        if (isGrounded()) {
            moveSettings.state = PlayerState.Run;

            var em = sweat.emission; // Sweat particle on
            em.enabled = true;

            rb.drag = moveSettings.rigidbodyDrag; // Make it harder to control
            moveSettings.curMoveSpeed = moveSettings.sprintSpeed; // Make it faster

            // ----- COMMENTED TO TEST JUMP + SPRINT
            if (isGrounded()) {
                moveSettings.jumpCounter = 2;
            }

            if (carryScript.IsCarrying)
                Drop(carryDrop.transform.position);
        }
    }

    public void Slide(bool _sliding) {
        sliding = _sliding;

        if (sliding) {
            rb.drag = moveSettings.rigidbodyDrag;
        }
        else {
            rb.drag = moveSettings.defaultDrag;
        }
    }

    void LStep() {
        AudioManager.instance.MakeNoise(stepSound, this.gameObject);
        stepParticlesLeft.Emit(1);
    }
    void RStep() {
        AudioManager.instance.MakeNoise(stepSound, this.gameObject);
        stepParticlesRight.Emit(1);
    }

    private bool TeleportChecker() {
        return Physics.Raycast(transform.position, Vector3.down, 5f, teleporterMask);
    }

    private void OnTriggerStay(Collider other) {
        if (other.gameObject.tag == "Glue" && other.gameObject.GetComponent<Glue>().State == GlueState.Fixed) {
            isGlued = true;
            if (jumpInput) {
                Debug.Log("We JUMPED OUT OF THE GLUE!!!!");
                other.gameObject.GetComponent<Glue>().Unglue();
                moveSettings.jumpCounter = 1;
                moveSettings.verticalVelocity = moveSettings.jumpForce + 2f; // Raise the vertical velocity to be equal to JumpForce (Jump)
                isGlued = false;
            }
        }

        //if (other.gameObject.tag == "CageCheckpoint") {
        //    if (InputManager.InteractButton(false) && other.GetComponent<ActivateCheckpoint>().isActivated && !carryScript.IsCarrying) {
        //        carryScript.carryObject.GetComponent<CageCheckpoint>().returnPoint = other.transform.GetChild(0);
        //        carryScript.carryObject.GetComponent<CageCheckpoint>().ResetPosition();
        //    }
        //}
    }

    private void OnTriggerExit(Collider other) {
        if (other.gameObject.tag == "Glue") {
            isGlued = false;
        }
    }

    private void OnCollisionEnter(Collision collision) {
        Slide(false); //must always stop sliding

        if (collision.gameObject.tag == "Enemy") {
            if (carryScript.IsCarrying)
                Drop(carryDrop.transform.position);

            StartCoroutine(PlayerDeath());
        }
    }

    //AT THE MOMENT IT CAN ONLY THROW CAGES, BUT WILL BE UPGRADED AS SOON AS THE INVENTORY SYSTEM GETS A LITTLE REFACTORING
    void Throw() {
        lastTargetPosition = moveSettings.throwTarget.transform.position; //target position must be stored before player rotating
        playerModel.transform.LookAt(new Vector3(moveSettings.throwTarget.transform.position.x, playerModel.transform.position.y, moveSettings.throwTarget.transform.position.z)); //player must look at the direction of the throw
        throwing = true;
        LockPlayer();
    }

    public void ThrowEvent() {
        Rigidbody rb = carryScript.CarryObject.GetComponent<Rigidbody>();
        Vector3 throwObject = carryScript.CarryObject.transform.position;

        Drop(carryPosition.transform.position); //in case of the cage

        Physics.gravity = Vector3.up * moveSettings.throwGravity;
        rb.velocity = CalculateLaunchData(throwObject, lastTargetPosition).initialVelocity;

        throwing = false;
        UnlockPlayer();
    }

    ThrowData CalculateLaunchData(Vector3 throwObject, Vector3 target) {
        float h = moveSettings.throwHeight;
        float g = moveSettings.throwGravity;

        float displacementY = target.y - throwObject.y;
        Vector3 displacementXZ = new Vector3(target.x - throwObject.x, 0, target.z - throwObject.z);

        float time = (Mathf.Sqrt(-2 * h / g) + Mathf.Sqrt(2 * (displacementY - h) / g));
        Vector3 velocityY = Vector3.up * Mathf.Sqrt(-2 * g * h);
        Vector3 velocityXZ = displacementXZ / time;

        return new ThrowData(velocityXZ + velocityY * -Mathf.Sign(g), time);
    }

    struct ThrowData {
        public readonly Vector3 initialVelocity;
        public readonly float timeToTarget;

        public ThrowData(Vector3 _initialVelocity, float _timeToTarget) {
            this.initialVelocity = _initialVelocity;
            this.timeToTarget = _timeToTarget;
        }
    }

    public void Drop(Vector3 dropPlace) {
        if (carryScript.IsCarrying) {
            carryScript.Drop(dropPlace);
        }
    }

    public IEnumerator PlayerDeath() {

        panel.GetComponent<Animator>().SetTrigger("Death");
        this.transform.SetParent(null);

        yield return new WaitForSeconds(0.2f);

        CheckpointManager.instance.LoadCheckpoint();
        playDeath = true;
    }

    public bool isGrounded() {
        #region raycasts bleh
        Ray rayCenter = new Ray(transform.position, transform.up);
        Ray rayLeft = new Ray(new Vector3(transform.position.x + 0.2f, transform.position.y, transform.position.z), -transform.up);
        Ray rayRight = new Ray(new Vector3(transform.position.x - 0.2f, transform.position.y, transform.position.z), -transform.up);
        Ray rayUp = new Ray(new Vector3(transform.position.x, transform.position.y, transform.position.z - 0.2f), -transform.up);
        Ray rayDown = new Ray(new Vector3(transform.position.x, transform.position.y, transform.position.z + 0.2f), -transform.up);

        Ray diagonalUpLeft = new Ray(new Vector3(transform.position.x + 0.2f, transform.position.y, transform.position.z + 0.2f), -transform.up);
        Ray diagonalUpRight = new Ray(new Vector3(transform.position.x - 0.2f, transform.position.y, transform.position.z + 0.2f), -transform.up);
        Ray diagonalDownLeft = new Ray(new Vector3(transform.position.x + 0.2f, transform.position.y, transform.position.z - 0.2f), -transform.up);
        Ray diagonalDownRight = new Ray(new Vector3(transform.position.x - 0.2f, transform.position.y, transform.position.z - 0.2f), -transform.up);

        Ray SmalldiagonalUpLeft = new Ray(new Vector3(transform.position.x + 0.5f, transform.position.y, transform.position.z + 0.5f), -transform.up);
        Ray SmalldiagonalUpRight = new Ray(new Vector3(transform.position.x - 0.5f, transform.position.y, transform.position.z + 0.5f), -transform.up);
        Ray SmalldiagonalDownLeft = new Ray(new Vector3(transform.position.x + 0.5f, transform.position.y, transform.position.z - 0.5f), -transform.up);
        Ray SmalldiagonalDownRight = new Ray(new Vector3(transform.position.x - 0.5f, transform.position.y, transform.position.z - 0.5f), -transform.up);

        RaycastHit hit;

        float rayDistance = 2f;

        // ------------- SIDE RAYCASTS ------------------
        Debug.DrawLine(transform.position, transform.position + transform.up * rayDistance, Color.green);
        Debug.DrawLine(new Vector3(transform.position.x + 0.2f, transform.position.y, transform.position.z), new Vector3(transform.position.x + 0.2f, transform.position.y, transform.position.z) - transform.up * rayDistance, Color.green);
        Debug.DrawLine(new Vector3(transform.position.x - 0.2f, transform.position.y, transform.position.z), new Vector3(transform.position.x - 0.2f, transform.position.y, transform.position.z) - transform.up * rayDistance, Color.green);
        // --------------------- UP RAYCASTS ------------------
        Debug.DrawLine(new Vector3(transform.position.x, transform.position.y, transform.position.z + 0.2f), new Vector3(transform.position.x, transform.position.y, transform.position.z + 0.2f) - transform.up * rayDistance, Color.green);
        Debug.DrawLine(new Vector3(transform.position.x, transform.position.y, transform.position.z - 0.2f), new Vector3(transform.position.x, transform.position.y, transform.position.z - 0.2f) - transform.up * rayDistance, Color.green);
        // --------------- DIAGONAL RAYCASTS --------------------
        Debug.DrawLine(new Vector3(transform.position.x + 0.2f, transform.position.y, transform.position.z + 0.2f), new Vector3(transform.position.x + 0.2f, transform.position.y, transform.position.z + 0.2f) - transform.up * rayDistance, Color.green);
        Debug.DrawLine(new Vector3(transform.position.x - 0.2f, transform.position.y, transform.position.z + 0.2f), new Vector3(transform.position.x - 0.2f, transform.position.y, transform.position.z + 0.2f) - transform.up * rayDistance, Color.green);
        Debug.DrawLine(new Vector3(transform.position.x + 0.2f, transform.position.y, transform.position.z - 0.2f), new Vector3(transform.position.x + 0.2f, transform.position.y, transform.position.z - 0.2f) - transform.up * rayDistance, Color.green);
        Debug.DrawLine(new Vector3(transform.position.x - 0.2f, transform.position.y, transform.position.z - 0.2f), new Vector3(transform.position.x - 0.2f, transform.position.y, transform.position.z - 0.2f) - transform.up * rayDistance, Color.green);
        // ----------------- SMALLER DIAGONAL RAYCASTS ------------
        Debug.DrawLine(new Vector3(transform.position.x + 0.8f, transform.position.y, transform.position.z + 0.8f), new Vector3(transform.position.x + 0.8f, transform.position.y, transform.position.z + 0.8f) - transform.up * rayDistance, Color.green);
        Debug.DrawLine(new Vector3(transform.position.x - 0.8f, transform.position.y, transform.position.z + 0.8f), new Vector3(transform.position.x - 0.8f, transform.position.y, transform.position.z + 0.8f) - transform.up * rayDistance, Color.green);
        Debug.DrawLine(new Vector3(transform.position.x + 0.5f, transform.position.y, transform.position.z - 0.5f), new Vector3(transform.position.x + 0.5f, transform.position.y, transform.position.z - 0.5f) - transform.up * rayDistance, Color.green);
        Debug.DrawLine(new Vector3(transform.position.x - 0.5f, transform.position.y, transform.position.z - 0.5f), new Vector3(transform.position.x - 0.5f, transform.position.y, transform.position.z - 0.5f) - transform.up * rayDistance, Color.green);

        if (Physics.Raycast(rayCenter, out hit, rayDistance) || Physics.Raycast(rayLeft, out hit, rayDistance) || Physics.Raycast(rayRight, out hit, rayDistance) || Physics.Raycast(rayUp, out hit, rayDistance) || Physics.Raycast(rayDown, out hit, rayDistance)
             || Physics.Raycast(diagonalDownLeft, out hit, rayDistance) || Physics.Raycast(diagonalDownRight, out hit, rayDistance) || Physics.Raycast(diagonalUpLeft, out hit, rayDistance) || Physics.Raycast(diagonalUpRight, out hit, rayDistance)
             || Physics.Raycast(SmalldiagonalDownLeft, out hit, rayDistance) || Physics.Raycast(SmalldiagonalDownRight, out hit, rayDistance) || Physics.Raycast(SmalldiagonalUpLeft, out hit, rayDistance) || Physics.Raycast(SmalldiagonalUpRight, out hit, rayDistance)) {
            return true;
        }
        else
            return false;
        #endregion
    }

    public void LockPlayer(bool lockRigidbody = true) {
        //stops Player movement
        isLocked = true;
        rb.mass = 10000f;
        moveSettings.curMoveSpeed = 0f;

        //nullifies Player speed at the moment
        rb.velocity = new Vector3(0, 0, 0);
        verticalInput = 0f;
        horizontalInput = 0f;

        //resets animation
        moveSettings.state = PlayerState.Idle;

        //must lock on cutscenes
        if (lockRigidbody) {
            rb.isKinematic = true;
        }
    }

    public void UnlockPlayer(bool unlockRigidbody = true) {
        //starts Player movement
        isLocked = false;
        rb.mass = 1;
        moveSettings.curMoveSpeed = moveSettings.defaultMoveSpeed;

        //must lock on cutscenes
        if (unlockRigidbody) {
            rb.isKinematic = false;
        }
    }
    #endregion

    void DrawPath() {
        ThrowData launchData = CalculateLaunchData(carryScript.CarryObject.transform.position, moveSettings.throwTarget.transform.position);
        Vector3 previousDrawPoint = carryScript.CarryObject.transform.position;

        int resolution = 30;
        for (int i = 0; i <= resolution; i++) {
            float simulationTime = i / (float)resolution * launchData.timeToTarget;
            Vector3 displacement = launchData.initialVelocity * simulationTime + Vector3.up * moveSettings.throwGravity * simulationTime * simulationTime / 2f;
            Vector3 drawPoint = carryScript.CarryObject.transform.position + displacement;

            Debug.DrawLine(previousDrawPoint, drawPoint, Color.red);
            previousDrawPoint = drawPoint;
        }
    }
}