﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class TargetDropShadow : MonoBehaviour {
    [SerializeField] Camera camera;

    PlayerModel player;
    SpriteRenderer target;
    float distFromPlayer;
    Vector3 origin;

    void Start() {
        player = GetComponentInParent<PlayerModel>();
        target = GetComponent<SpriteRenderer>();
        distFromPlayer = Vector3.Distance(transform.position, player.transform.position);
    }

    void Update() {
        if (player.carryHitbox.GetComponent<CarryingObject>().IsCarrying) {
            Vector3 cameraDirection = camera.transform.TransformDirection(Vector3.forward);
            origin = player.transform.position + new Vector3(cameraDirection.x, player.transform.TransformDirection(Vector3.forward).y, cameraDirection.z) * distFromPlayer;

            Ray ray = new Ray(origin + Vector3.up * 10f, Vector3.down * 60f);
            RaycastHit hit;            

            if (Physics.Raycast(ray, out hit, 60f, player.moveSettings.groundLayers)) {
                target.enabled = true;
                transform.position = hit.point + new Vector3(0, .125f, 0);
            }
            else {
                target.enabled = false;
                transform.position = origin;
            }
        }
    }

    void OnDrawGizmos() {
        Debug.DrawRay(origin + Vector3.up * 10f, Vector3.down * 60f, Color.green);
    }
}
