﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class PlayerView : MonoBehaviour {
    PlayerModel player;
    Animator anim;

    PlayerState oldState;
    float moveLerp, vel;

    #region METHODS
    void Start() {
        player = GetComponent<PlayerModel>();
        anim = GetComponent<Animator>();
    }

    void Update() {
        Debug.Log("Animator -> " + player.Throwing);
        anim.SetBool("throwing", player.Throwing);

        if (player.carryScript.IsCarrying) {
            anim.SetFloat("hasCage 0", 1);
        }
        else {
            anim.SetFloat("hasCage 0", 0);
        }

        if (player.isGrounded()) {
            anim.SetInteger("State", (int)player.moveSettings.state);
        }

        anim.SetBool("isMoving", player.IsMoving);
        anim.SetFloat("verticalVelocity", player.rb.velocity.y);

        if (player.isGrounded()) {
            anim.SetBool("isGrounded", true);
        }
        else {
            anim.SetBool("isGrounded", false);
        }

        if (player.moveSettings.jumpCounter == 2 && player.moveSettings.verticalVelocity > 0 && !player.carryScript.IsCarrying) {
            anim.SetBool("isDoubleJumping", true);
        }
        else {
            anim.SetBool("isDoubleJumping", false);
        }
    }
    #endregion
}
