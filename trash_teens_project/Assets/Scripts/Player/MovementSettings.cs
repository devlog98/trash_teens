﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MoveSettings
{
    [Header("Player Data")]
    public Transform playerModel;
    public PlayerState state;

    [Space]

    [Header("Jump Variabes")]
    public float gravity = 14.0f;
    public float jumpForce = 10.0f;
    public float defaultJumpForce = 10.0f;
    public float verticalVelocity;
    public float maxVerticalVelocity = -14f;
    public LayerMask groundLayers;
    public int jumpCounter = 0;
    public int jumpMaxCounter = 2;

    [Space]

    [Header("Movement Variables")]
    public float defaultMoveSpeed = 4f;
    public float curMoveSpeed = 4f;
    public float rotateSpeed = 20.5f;

    [Space]

    [Header("Sneak Variables")]
    public float sneakAmount = 2.5f;

    [Space]

    [Header("Carry Variables")]
    public float carrySpeed = 2.5f;
    public Transform carryHitbox;

    [Space]

    [Header("Sprint Variables")]
    public float sprintSpeed = 7f;
    public float rigidbodyDrag = 3f;
    public float defaultDrag = 7.5f;

    [Space]

    [Header("Throw Variables")]
    public SpriteRenderer throwTarget;
    public float throwHeight = 25;
    public float throwGravity = -18;

    public Vector3 movement;
    [HideInInspector]
    public float x, z;
}