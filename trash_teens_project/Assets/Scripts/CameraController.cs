﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public Transform target;
    public GameObject itemWheel;

    [System.Serializable]
    public class PositionSettings
    {
        public Vector3 targetPosOffset = new Vector3(0, 3.4f, 0); // Offset the camera 
        public float lookSmooth = 100f; // Smoothing the camera
        public float distanceFromTarget = -8; // Default camera distance (Modified by zoom)
        public float zoomSmooth = 100; // How fast you can zoom in or out
        public float maxZoom = -2; // How close you can get to the target
        public float minZoom = -15; // How far away you can get to the target
        public bool smoothFollow = true; // Option to make the camera follow smoothly
        public float smooth = 0.05f; // How smoothly it will follow

        [HideInInspector]
        public float newDistance = -8; // set by zoom input
        [HideInInspector]
        public float adjustmentDistance = -8; // changing the distance from the player when there is a collision
    }

    [System.Serializable]
    public class OrbitSettings
    {
        public float xRotation = -20; // Inputing the X rotation 
        public float yRotation = -180; // Inputing the Y rotation
        public float maxXRotation = 25; // Maximum vertical Rotation
        public float minXRotation = -85; // Minimum vertical Rotation
        public float vOrbitSmooth = 150; // Rotation on the X Axis Smoothing (Vertical!!!)
        public float hOrbitSmooth = 150; // Rotation on the Y Axis Smoothing (Horizontal!!!)
    }

    [System.Serializable]
    public class InputSettings
    {
        public string ORBIT_HORIZONTAL_SNAP = "OrbitHorizontalSnap"; // Input that snaps the Y Rotation back behind the target
        public string ORBIT_HORIZONTAL = "Mouse X"; // Input that rotates the camera on the Y Axis
        public string ORBIT_VERTICAL = "Mouse Y"; // Input that rotates the camera on the X Axis
        public string ZOOM = "Mouse ScrollWheel"; // Input that zooms the camera
    }

    [System.Serializable]
    public class DebugSettings
    {
        public bool drawDesiredCollisionLines = true; // Drawing the desired collision lines from the target to the clip points 
        public bool drawAdjustedCollisionLines = true; // Drawing the adjusted collision lines from the target to the clip points 
    }

    //References to classes
    public PositionSettings position = new PositionSettings();
    public OrbitSettings orbit = new OrbitSettings();
    public InputSettings input = new InputSettings();
    public DebugSettings debug = new DebugSettings();
    public CameraCollision collision = new CameraCollision();


    Vector3 targetPos = Vector3.zero; // Position of the target
    Vector3 destination = Vector3.zero; // Destination the camera will move to
    Vector3 adjustedDestination = Vector3.zero; // Taking control from normal destination when we're colliding
    Vector3 camVel = Vector3.zero; // Camera velocity if we are using our smoothing option
    float vOrbitInput, hOrbitInput, zoomInput, hOrbitSnapInput; // Player Inputs (vertical, horizontal, zoom, and snap)

    public Transform pivot;
    public float rotateSpeed = 1f;

    void Start()
    {
        SetCameraTarget(target);

        //Starting off the camera in the right position
        MoveToTarget();

        collision.Initialize(Camera.main); // passing camera to the method

        // Filling our clip point arrays
        collision.UpdateCameraClipPoints(transform.position, transform.rotation, ref collision.adjustedCameraClipPoints); // adjusted camera clip points are a reference to our camera curr position
        collision.UpdateCameraClipPoints(destination, transform.rotation, ref collision.desiredCameraClipPoints); // Setting our desired clip points

        // Making the cursor invisible
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        pivot.transform.position = target.transform.position;
        pivot.transform.parent = null;
    }

    public void SetCameraTarget(Transform t)
    {
        // Setting the target
        target = t;

        if (target != null)
        {

        }
        else
            Debug.LogError("No target on camera"); // Alert if we don't have a target
    }

    // Method that gets the player input
    void GetInput()
    {
        //if (itemWheel.activeInHierarchy == false)
        //{
            vOrbitInput = InputManager.HorizontalCamera();
            hOrbitInput = InputManager.VerticalCamera();
            hOrbitSnapInput = Input.GetAxisRaw(input.ORBIT_HORIZONTAL_SNAP);
            zoomInput = Input.GetAxisRaw(input.ZOOM);
        //}
    }

    // Being called every frame
    void Update()
    {


        // Getting player input
        GetInput();

        // Orbiting Target (if player input exists)
        OrbitTarget();

        // Zooming in on Target (if player input exists)
        ZoomInOnTarget();
    }

    void LateUpdate()
    {
        pivot.transform.position = target.transform.position;

        //Moving towards Target
        MoveToTarget();

        //Looking towards Target
        LookAtTarget();

        // Rotating the playerPivot
        PivotRotate();

    }
    // Being called every physics frame (since we're using raycasts)
    void FixedUpdate()
    {

        // Updating our clip point arrays every frame
        collision.UpdateCameraClipPoints(transform.position, transform.rotation, ref collision.adjustedCameraClipPoints); // adjusted camera clip points are a reference to our camera curr position
        collision.UpdateCameraClipPoints(destination, transform.rotation, ref collision.desiredCameraClipPoints); // Setting our desired clip points

        //Draw debug lines
        for (int i = 0; i < 5; i++)//Looping through our clip point array (which has 5 values)
        {
            if (debug.drawDesiredCollisionLines) // if our Debug settings has drawing desired lines set to true
            {
                Debug.DrawLine(targetPos, collision.desiredCameraClipPoints[i], Color.white); // Starts at target Position, moves to the clip point, colors white
            }
            if (debug.drawAdjustedCollisionLines) // if our Debug settings has drawing adjusted lines set to true
            {
                Debug.DrawLine(targetPos, collision.adjustedCameraClipPoints[i], Color.green); // Starts at target Position, moves to the clip point, colors green
            }
        }

        collision.checkColliding(targetPos); // using raycasts here, this is what modifies the colliding boolean
        position.adjustmentDistance = collision.getAdjustedDistanceWithRayFrom(targetPos); // updates our adjustment distance from targetPos
    }

    // Moving towards target
    void MoveToTarget()
    {
        targetPos = target.position + position.targetPosOffset;
        destination = Quaternion.Euler(orbit.xRotation, orbit.yRotation, 0) * -Vector3.forward * position.distanceFromTarget;
        destination += targetPos;

        if (collision.colliding) // if we're colliding
        {
            adjustedDestination = Quaternion.Euler(orbit.xRotation, orbit.yRotation, 0) * Vector3.forward * position.adjustmentDistance;
            adjustedDestination += targetPos;

            if (position.smoothFollow) //if we have the smooth follow option on
            {
                // use Smooth Damp funciton
                //transform.position = Vector3.SmoothDamp(transform.position, adjustedDestination, ref camVel, position.smooth);
                transform.position = Vector3.Lerp(transform.position, adjustedDestination, position.smooth * Time.deltaTime);
            }
            else
                transform.position = adjustedDestination; // if we dont have smooth follow, just instantly move to our adjusted destination
        }
        else // if we're not colliding, doing the same thing, but instead of adjustedDestination, use Destination
        {
            if (position.smoothFollow) //if we have the smooth follow option on
            {
                // use Smooth Damp funciton
                //transform.position = Vector3.SmoothDamp(transform.position, destination, ref camVel, position.smooth);
                transform.position = Vector3.Lerp(transform.position, destination, position.smooth * Time.deltaTime);
            }
            else
                transform.position = destination; // if we dont have smooth follow, just instantly move to our adjusted destination

        }
    }

    // Rotating the camera to look towards the target
    void LookAtTarget()
    {
        Quaternion targetRotation = Quaternion.LookRotation(targetPos - transform.position);
        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, position.lookSmooth * Time.deltaTime);
    }

    void OrbitTarget()
    {
        // If we press the Camera Snap button, return to the character's back
       if (hOrbitSnapInput > 0)
        {
            orbit.yRotation = -180;
        }

        // Adding the rotation based on the input of the player * the smoothing value
        orbit.xRotation += vOrbitInput * orbit.vOrbitSmooth * Time.deltaTime;
        orbit.yRotation += hOrbitInput * orbit.vOrbitSmooth * Time.deltaTime;

        //Debug.Log("Camera info: " + orbit.xRotation + ", " + orbit.yRotation);

        // If orbit X rotation (Vertical!!!) is higher than the max value or lower than the min value
        if (orbit.xRotation > orbit.maxXRotation)
        {
            orbit.xRotation = orbit.maxXRotation;
        }
        if (orbit.xRotation < orbit.minXRotation)
        {
            orbit.xRotation = orbit.minXRotation;
        }
    }

    void ZoomInOnTarget()
    {
        // Lower the distance to the target based on the zoom input
        position.distanceFromTarget += zoomInput * position.zoomSmooth * Time.deltaTime;

        //Checking if we reached min or max zoom
        if (position.distanceFromTarget > position.maxZoom)
        {
            position.distanceFromTarget = position.maxZoom;
        }
        if (position.distanceFromTarget < position.minZoom)
        {
            position.distanceFromTarget = position.minZoom;
        }
    }

    void PivotRotate()
    {
            pivot.transform.rotation = Quaternion.Euler(0f, transform.rotation.eulerAngles.y, 0f);
    }
}
