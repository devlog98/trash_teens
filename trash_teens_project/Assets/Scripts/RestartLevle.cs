﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RestartLevle : MonoBehaviour {

	void Update () {
		if(Input.GetKeyDown(KeyCode.P))
        {
            LoadManager.instance.TransitionToScene(0);
        }
	}
}
