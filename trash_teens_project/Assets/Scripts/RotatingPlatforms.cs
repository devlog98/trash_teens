﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatingPlatforms : MonoBehaviour, IActivatable {

    public enum axisToRotate { Yaw, Pitch, Roll}

    public Transform pivot;
    //public int axisToRotate; // If 1: X, 2: Y, 3: Z;
    public float speed;

    public bool isActivated;
    public bool ReturnsToOriginalPos;

    public ActivatablePropertie activatableProperty;
    // If you want to just keep rotating
    public bool rotateForever = true;

    // If you want to rotate just a set amount
    public float timeToRotate;
    float curTime;
    float returnCurTime;

    private bool readyToReturn;

    [Tooltip ("Roll = X, Yaw = z, Pitch = Z")]
    public axisToRotate axis;

    void Start()
    {
        returnCurTime = timeToRotate + 1;
    }
	
	// Update is called once per frame
	void Update () {

        if (activatableProperty.activated)
        {
            returnCurTime = 0;
            if (rotateForever || timeToRotate >= curTime)
            {
                if (axis == axisToRotate.Roll)
                    transform.RotateAround(pivot.position, Vector3.right, speed * Time.deltaTime); // Roll


                if (axis == axisToRotate.Yaw)
                    transform.RotateAround(pivot.position, Vector3.up, speed * Time.deltaTime); // Yaw


                if (axis == axisToRotate.Pitch)
                    transform.RotateAround(pivot.position, Vector3.forward, speed * Time.deltaTime); // Pitch


                curTime += Time.deltaTime;
                readyToReturn = true;
            }
        }
        else if(ReturnsToOriginalPos && readyToReturn)
        {
            curTime = 0;
            Debug.Log("SHhuold return");
            if (rotateForever || timeToRotate >= returnCurTime)
            {
                Debug.Log("Returning active: " + activatableProperty.activated + "curtime: " + curTime);
                if (axis == axisToRotate.Roll)
                    transform.RotateAround(pivot.position, Vector3.right, -speed * Time.deltaTime); // Roll


                if (axis == axisToRotate.Yaw)
                    transform.RotateAround(pivot.position, Vector3.up, -speed * Time.deltaTime); // Yaw


                if (axis == axisToRotate.Pitch)
                    transform.RotateAround(pivot.position, Vector3.forward, -speed * Time.deltaTime); // Pitch


                returnCurTime += Time.deltaTime;
            }
        }
    }

    public void Activate(float activationParameter)
    {
        if (activationParameter == 0f && (curTime == 0 && returnCurTime >= timeToRotate))
            activatableProperty.activated = true;

        else if (activationParameter == 1f && (returnCurTime == 0) && curTime >= timeToRotate)
            activatableProperty.activated = false;

        /*else if (activationParameter == 2f)
        {
            if (isActivated && (returnCurTime == 0))
                isActivated = false;
            else if (!isActivated && curTime == 0)
                isActivated = true;
        }*/
    }
}
