﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootBasketball : MonoBehaviour {

    public GameObject ball;
    public Transform shootPos;
    public Vector3 force;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerStay(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            if(InputManager.InteractButton(false))
            {
                ball.GetComponent<Rigidbody>().velocity = Vector3.zero;
                ball.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
                ball.SetActive(true);
                ball.transform.position = shootPos.transform.position;
                ball.GetComponent<Rigidbody>().AddForce(force);
            }
        }
    }
}
