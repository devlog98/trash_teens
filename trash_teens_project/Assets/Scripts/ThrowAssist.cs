﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowAssist : MonoBehaviour {

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Carryable")
        {
            other.transform.SetParent(this.transform);

            other.GetComponent<Rigidbody>().isKinematic = true;

            other.GetComponent<Rigidbody>().velocity = Vector3.zero;
            other.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
        }
    }
}
