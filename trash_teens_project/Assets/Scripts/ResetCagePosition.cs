﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetCagePosition : MonoBehaviour {

    public Transform originalPosition;
    public Transform defaultOriginalPosition;
    private bool isInsideEnemyArea;
    public bool shouldGoBack = true;
    public float timeToReturn = 5f;
    private float elapsedTime = 0f;

    private void Start()
    {
        defaultOriginalPosition = originalPosition;
    }

    // Update is called once per frame
    void Update () {
            ReturnToPosition();
	}

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Enemy")
        {
            isInsideEnemyArea = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            isInsideEnemyArea = false;
        }
    }

    void ReturnToPosition()
    {
        if (isInsideEnemyArea)
        {
            if (GetComponent<Rigidbody>().isKinematic == false && transform.position != originalPosition.position)
            {
                if (elapsedTime >= timeToReturn)
                {
                    transform.position = defaultOriginalPosition.position;
                    GetComponent<Rigidbody>().velocity = Vector3.zero;
                    GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
                    elapsedTime = 0;
                }
                else
                    elapsedTime += Time.deltaTime;
            }
        }

    }

}
