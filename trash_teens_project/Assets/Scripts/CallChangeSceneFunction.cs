﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CallChangeSceneFunction : MonoBehaviour
{
    // Script to call functions from other scripts with Animation Events.

    public LevelComplete levelCompleteScript;

    public void callLevelComplete()
    {
        levelCompleteScript.FinishLevel();
    }
}
