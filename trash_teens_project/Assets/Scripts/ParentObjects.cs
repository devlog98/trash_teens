﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParentObjects : MonoBehaviour {


    /* --------- HOW TO USE --------
     * Put this one on the model, not the Empty Parent GameObject
     * also, make sure the model has a trigger collider, and that it is very small (So it doesnt touch the grabbing hitbox of the player)
     */
    public GameObject parentTo;
    public LayerMask ignoreMask;

    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.tag != "NoParent" && other.gameObject.layer != (int)Mathf.Log(ignoreMask.value, 2)) {
            other.transform.SetParent(parentTo.transform, true);
        }
    }

    private void OnTriggerExit(Collider other) {
        if (other.transform.parent == parentTo.transform && other.gameObject.layer != (int)Mathf.Log(ignoreMask.value, 2)) {
            other.transform.SetParent(null, true);
        }
    }
}
