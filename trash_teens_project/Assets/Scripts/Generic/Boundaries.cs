﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boundaries : MonoBehaviour
{
    [SerializeField] Vector3 boundsSize;
    [SerializeField] Color debugColor = new Vector4(0, 0, 0, 1);

    Bounds bounds;
    public Bounds Bounds { get { return bounds; } }
    public Vector3 Center { 
        get { return bounds.center; }
        set { bounds.center = value; }
    }

    void Awake()
    {
        bounds = new Bounds(transform.position, new Vector3(boundsSize.x, boundsSize.y, boundsSize.z));
    }

    void OnDrawGizmos()
    {
        Gizmos.color = debugColor;

        if (bounds.size.magnitude == 0)
        {
            Gizmos.DrawWireCube(transform.position, boundsSize);
        }
        else
        {
            Gizmos.DrawWireCube(bounds.center, bounds.size);
        }
    }
}