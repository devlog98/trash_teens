﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//player progress from save file
public class ProgressData {
    public string filename;
    public List<LevelData> completedLevels;
    public float totalPlaytime;
    //public List<CardData> collectedCards; // to be implemented in the future

    public ProgressData(string _filename, List<LevelData> _completedLevels, float _totalPlaytime) {
        filename = _filename;
        completedLevels = _completedLevels;
        totalPlaytime = _totalPlaytime;
    }
}