﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//helper class for data serialization on save
[System.Serializable]
public class SaveData {
    public string filename;
    public string[] completedLevelNames;
    public float[] completedLevelTimes;
    public float totalPlaytime;

    public SaveData(ProgressData progress) {
        //storing filename
        filename = progress.filename;

        //storing all level data information
        int length = progress.completedLevels.Count;

        completedLevelNames = new string[length];
        completedLevelTimes = new float[length];

        for (int i = 0; i < length; i++) {
            completedLevelNames[i] = progress.completedLevels[i].name;
            completedLevelTimes[i] = progress.completedLevels[i].time;
        }

        //storing total time played
        totalPlaytime = progress.totalPlaytime;
    }
}