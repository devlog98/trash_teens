﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//level completion data
public class LevelData {
    public string name;
    public float time;

    public LevelData(string _name, float _time) {
        name = _name;
        time = _time;
    }
}