﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//collected cards data
public class CardData : MonoBehaviour {
    public string name;

    public CardData(string _name) {
        name = _name;
    }
}