﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(DontDestroy))]
public class ProgressionManager : MonoBehaviour {
    //static instance can be called any time
    public static ProgressionManager instance;

    ProgressData progress;
    Timer totalPlaytimeTimer;

    public List<LevelData> CompletedLevels { get { return progress.completedLevels; } }
    public float TotalPlaytime { get { return progress.totalPlaytime; } }    

    //avoids multiple instances running at same time
    void Awake() {
        if (instance != null && instance != this) {
            Destroy(this.gameObject);
        }
        else {
            instance = this;
        }
    }

    void Start() {
        totalPlaytimeTimer = new Timer();
        LoadProgress(SaveSystem.filenameBase + "0"); //just for testing
    }

    //save progress when new game created
    public void SaveProgress(string filename) {
        List<LevelData>completedLevels  = new List<LevelData>();
        float totalPlaytime = 0;
        progress = new ProgressData(filename, completedLevels, totalPlaytime);

        SaveSystem.SaveGame(progress);

        totalPlaytimeTimer.StartTimer();
        Debug.Log("Progress saved at " + progress.filename);
    }

    //save progress when exitting game
    public void SaveProgress() {
        progress.totalPlaytime += totalPlaytimeTimer.StopTimer();

        SaveSystem.SaveGame(progress);

        totalPlaytimeTimer.StartTimer();
        Debug.Log("Progress saved at " + progress.filename);
    }

    //save progress when level completed
    public void SaveProgress(string levelName, float levelTime) {
        progress.totalPlaytime += totalPlaytimeTimer.StopTimer();

        bool levelExists = false;
        //update level stats if player already finished it
        foreach (LevelData level in progress.completedLevels) {
            if (level.name == levelName) {
                level.time = levelTime;
                levelExists = true;
                break;
            }
        }

        //if player never finished the level, add it to the list
        if (!levelExists) {
            progress.completedLevels.Add(new LevelData(levelName, levelTime));
        }

        SaveSystem.SaveGame(progress);

        totalPlaytimeTimer.StartTimer();
        Debug.Log("Progress saved at " + progress.filename);
    }

    //load progress from file
    public void LoadProgress(string filename) {
        progress = SaveSystem.LoadGame(filename);

        if (progress == null) {
            SaveProgress(filename); //create new file progress if empty just for testing
        }

        totalPlaytimeTimer.StartTimer();
        Debug.Log("Progress loaded from " + progress.filename);
    }

    //how unlocking levels work:
    //every time player finishes a new level, it is added to the levels list (so it's size gets bigger)
    //this in turn will make the current level index higher, and on the Level Select you can change unlock indexes accordingly
    //in the beginning of the game the list is empty, so index == 0 and you can only access levels with unlock indexes <= 0 (in this case only tutorial)

    //how saving playtime work:
    //the Timer class will mark beginning and end time on specific save and load methods
    //then the value will be added to the totalPlaytime float on progress
}