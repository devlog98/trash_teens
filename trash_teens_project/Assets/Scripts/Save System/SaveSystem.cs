﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

//used to save and load game data anywhere
public static class SaveSystem {
    public static string filenameBase = "trashbin_";

    public static void SaveGame(ProgressData progress) {
        //sets formatter, path and file
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Path.Combine(Application.persistentDataPath, progress.filename + ".lo");
        FileStream stream = new FileStream(path, FileMode.Create);

        //encrypts data
        SaveData data = new SaveData(progress);
        formatter.Serialize(stream, data);

        //close file
        stream.Close();
    }

    public static ProgressData LoadGame(string filename) {
        //gets path
        string path = Path.Combine(Application.persistentDataPath, filename + ".lo");

        if (File.Exists(path)) {
            //if file exists set formatter and file
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            //decrypts data and close file
            SaveData data = formatter.Deserialize(stream) as SaveData;
            stream.Close();

            //organize data
            List<LevelData> completedLevels = new List<LevelData>();
            for (int i = 0; i < data.completedLevelNames.Length; i++) {
                completedLevels.Add(new LevelData(data.completedLevelNames[i], data.completedLevelTimes[i]));
            }

            float totalPlaytime = data.totalPlaytime;

            ProgressData progress = new ProgressData(filename, completedLevels, totalPlaytime);

            return progress;
        }
        else {
            //else alert
            Debug.LogError("Save file not found in " + path);
            return null;
        }
    }
}