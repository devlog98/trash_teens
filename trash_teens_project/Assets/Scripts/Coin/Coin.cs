﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MSuits.Collectables.Coin {
    public enum CoinValue { Low = 5, Medium = 25, High = 50 }

    [RequireComponent(typeof(BoxCollider))]
    public class Coin : MonoBehaviour {
        [SerializeField] LayerMask collisionMask; //select which layer masks can interact with the coin
        [SerializeField] CoinValue coinValue;

        private void OnTriggerEnter(Collider coll) {
            //when Player touches coin
            if (coll.gameObject.layer == Mathf.Log(collisionMask.value, 2)) {
                Destroy(this.gameObject);
                Debug.Log(coinValue);
            }
        }
    }
}