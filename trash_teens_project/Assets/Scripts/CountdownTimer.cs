﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CountdownTimer : MonoBehaviour
{
    public CountdownToActivation timer;
    private Text timerText;

    // Start is called before the first frame update
    void Start()
    {
        timerText = GetComponentInChildren<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        timerText.text = "" + Mathf.CeilToInt(timer.timeToDeactivate - timer.curTime);
    }
}
