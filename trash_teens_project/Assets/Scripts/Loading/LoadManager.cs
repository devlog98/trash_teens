﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadManager : MonoBehaviour {
    public static LoadManager instance; //static instance can be called any time

    [SerializeField] float transitionSmoothTime = .1f;
    int targetScene;
    CanvasGroup canvasGroup;

    //avoids multiple instances running at same time
    void Awake() {
        if (instance != null && instance != this) {
            Destroy(this.gameObject);
        } else {
            instance = this;
        }
    }

    void Start() {
        canvasGroup = GetComponent<CanvasGroup>();
    }

    public void TransitionToScene(int sceneIndex) {
        //can't load the same scene you're into
        if (SceneManager.GetActiveScene().buildIndex == sceneIndex)
            return;

        targetScene = sceneIndex;

        //turn the loading canvas on
        StopCoroutine("TransitionWithAlpha");
        StartCoroutine("TransitionWithAlpha", 1);
    }

    IEnumerator TransitionWithAlpha(float targetAlpha) {
        float diff = Mathf.Abs(canvasGroup.alpha - targetAlpha);
        float transitionRate = 0;

        //executes canvas alpha transition
        while (diff > 0.025f) {
            canvasGroup.alpha = Mathf.SmoothDamp(canvasGroup.alpha, targetAlpha, ref transitionRate, transitionSmoothTime);
            diff = Mathf.Abs(canvasGroup.alpha - targetAlpha);
            yield return new WaitForSeconds(Time.deltaTime); //mimics Update
        }

        canvasGroup.alpha = targetAlpha;

        //loads when canvas is being activated
        if (targetAlpha == 1) {
            StartCoroutine("LoadScene");
        }
    }

    IEnumerator LoadScene() {
        //start loading scene
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(targetScene);

        while (!asyncLoad.isDone) {
            yield return null;
        }

        //turn the loading canvas off
        StartCoroutine("TransitionWithAlpha", 0);
    }
}