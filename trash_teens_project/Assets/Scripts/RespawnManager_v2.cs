﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespawnableAttributes
{
   public bool isActive;
}

public class RespawnManager_v2 : MonoBehaviour
{
    public GameObject[] objectsToKeep;
    public bool[] objectsState;
    public GameObject player, cage;
    public Vector3 playerRespawnPoint;
    public Vector3 cageRespawnPoint;

    //[HideInInspector]
    //public Transform playerRespawnPoint = player.;
    //public Transform cageRespawnPoint;

    // Start is called before the first frame update
    void Start()
    {
        playerRespawnPoint = new Vector3(0,0,0);
        cageRespawnPoint = cage.transform.position;
        objectsState = new bool[objectsToKeep.Length];
    }

    public void SaveObjectState()
    {
        for(int i = 0; i < objectsToKeep.Length; i++)
        {
            objectsState[i] = objectsToKeep[i].GetComponent<ActivatablePropertie>().activated;
            Debug.Log("object + State: " + objectsToKeep[i].gameObject.name + objectsToKeep[i].GetComponent<ActivatablePropertie>().activated);
            Debug.Log("current state of the ObjectsState array: " + objectsState[i]);
        }
    }

    // ----- Saves player spawn Point
    public void SavePlayerState(Vector3 _position)
    {
        playerRespawnPoint = _position; 
    }

    // ----- Saves Cage spawn Point
    public void SaveCageState(Vector3 _position)
    {

        cageRespawnPoint = _position;
    }

    // Death State, Reset Objects to the State and Position they were saved
    public void ResetObjectState()
    {
        for (int i= 0; i < objectsToKeep.Length; i++)
        {
            Debug.Log("we're activating: " + objectsToKeep[i].gameObject.name + objectsToKeep[i].GetComponent<ActivatablePropertie>().activated);
            objectsToKeep[i].GetComponent<ActivatablePropertie>().activated = objectsState[i];
        }

        player.transform.position = playerRespawnPoint;
        cage.transform.position = cageRespawnPoint;
    }
}
