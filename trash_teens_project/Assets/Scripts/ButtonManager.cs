﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonManager : MonoBehaviour
{
    public GameObject[] objectToActivate;
    public List<ActivationHandler> buttons;

    bool lastState;

    [Tooltip("0: True, 1: False, 2: !IsActive")]
    [Range(0, 2)] public int activateParameter = 2;

    void Update() {
        if (ButtonsPressed()) {
            ExecuteAction();
            lastState = !lastState;
        }
    }

    public void ExecuteAction()
    {
        for (int i = 0; i < objectToActivate.Length; i++)
        {
            objectToActivate[i].SendMessage("Activate", activateParameter);
        }
    }

    private bool ButtonsPressed() {
        return !buttons.Find(x => x.isActive == lastState);
    }

    public void Deactivate() {
        foreach(ActivationHandler button in buttons) {
            button.isActive = false;
        }

        lastState = false;
    }
}