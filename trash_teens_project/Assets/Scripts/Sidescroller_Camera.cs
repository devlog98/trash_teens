﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sidescroller_Camera : MonoBehaviour
{

    public Transform target;
    public float smoothSpeed = 0.125f;
    public Vector3 offset;
    public bool smoothCamera;

    void LateUpdate()
    {
        Vector3 desiredPosition = target.transform.position + offset;

        if (smoothCamera)
        {
            transform.position = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed);
        }

        else
        {
            transform.position = desiredPosition;
        }
    }
}
