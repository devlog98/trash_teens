﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace MSuits.Enemy.Patrol {
    [Serializable]
    public class PatrolRoute {
        [SerializeField] PatrolPoint[] patrolPoints;
        public PatrolPoint[] Points { get { return patrolPoints; } }
    }
}