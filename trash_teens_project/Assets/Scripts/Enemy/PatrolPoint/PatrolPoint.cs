﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace MSuits.Enemy.Patrol {
    [Serializable]
    public class PatrolPoint {
        public Transform transform;
        public bool lookAround;
        public float initialRotation;
    }
}