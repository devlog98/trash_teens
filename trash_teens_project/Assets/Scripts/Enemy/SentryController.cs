﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MSuits.Enemy {
    [RequireComponent(typeof(SentryModel))]
    [RequireComponent(typeof(SentryView))]
    public class SentryController : MonoBehaviour {
        SentryModel enemy;
        SentryView anim;

        void OnEnable() {
            AudioManager.MakeNoiseEvent += HearEvent;
        }

        void OnDisable() {
            AudioManager.MakeNoiseEvent -= HearEvent;
        }

        void Start() {
            enemy = GetComponent<SentryModel>();
            anim = GetComponent<SentryView>();
        }

        void Update() {
            CheckMovementBounds();

            if (!enemy.IsWaiting) {
                if (!enemy.IsLocked) {
                    switch (enemy.State) {
                        case EnemyState.Unaware:
                            enemy.Patrol();                        
                            anim.Patrol();
                            break;
                        case EnemyState.Searching:
                            enemy.Search();
                            anim.Search(enemy.TargetRotation, enemy.Progress);
                            break;                    
                        case EnemyState.Alert:
                            enemy.Pursue();
                            anim.Pursue();
                            enemy.TargetInMovementBounds();
                            break;

                    }
                } else {
                    Debug.Log("Inimigo não pode mais se mover na plataforma!");
                }
            }

            if (enemy.State == EnemyState.Unaware) {
                anim.Patrol();
            }
            else if (enemy.State == EnemyState.Alert) {                
                anim.Pursue();
            }
        }

        public void CheckMovementBounds() {
            if (enemy.State != EnemyState.Alert) {
                if (enemy.TargetInMovementBounds()) {
                    UIManager.instance.ActivateRadar(true);
                    MusicManager.instance.SetMusicState("ignacio_state", 1);
                    enemy.See();
                }
                else {
                    UIManager.instance.ActivateRadar(false);
                    MusicManager.instance.SetMusicState("ignacio_state", 0);
                }
            }
            else {
                MusicManager.instance.SetMusicState("ignacio_state", 2);
            }
        }

        public void HearEvent(GameObject go) {
            if (enemy.State != EnemyState.Alert) {
                if (enemy.TargetInMovementBounds() && enemy.TargetInHearingBounds(go)) {
                    enemy.Hear(go);
                }
            }
        }
    }
}