﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MSuits.Enemy {
    [RequireComponent(typeof(Animator))]
    public class SentryView : MonoBehaviour {
        Animator anim;
        [SerializeField] GameObject rotationBone;

        void Start() {
            anim = GetComponent<Animator>();
        }

        public void Patrol() {            
            anim.SetBool("Alert", false);
            anim.SetBool("Searching", false);
        }

        public void Search(Quaternion targetRotation, float progress) {
            anim.SetBool("Searching", true);

            Quaternion currentRotation = rotationBone.transform.localRotation;

            //apply rotation slerp based on progress to achieve smoothness
            if (progress < 1 && progress >= 0)
            {
                //change rotation axis and direction because of joint default rotation
                targetRotation = Quaternion.Inverse(new Quaternion(targetRotation.y, targetRotation.x, targetRotation.z, targetRotation.w));
                rotationBone.transform.localRotation = Quaternion.Slerp(currentRotation, targetRotation, progress);
            }
        }

        public void Pursue() {
            rotationBone.transform.localRotation = Quaternion.Euler(Vector3.zero);
            anim.SetBool("Alert", true);
        }
    }
}