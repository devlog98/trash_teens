﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRadar : MonoBehaviour {
    [SerializeField] Transform player;
    [SerializeField] Transform enemy;

    void Update() {
        transform.LookAt(enemy);

        Vector3 distance = enemy.position - new Vector3(player.position.x, enemy.position.y, player.position.z);
        transform.position = enemy.position - distance / 6;

        Debug.DrawRay(player.position, distance * 10, Color.blue);
    }
}