﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using MSuits.Enemy.Patrol;

namespace MSuits.Enemy {
    public enum EnemyState { Unaware, Searching, Alert, Cautious, Victorious }

    public class SentryModel : MonoBehaviour
    {
        #region GENERAL
        BoxCollider coll;

        EnemyState state;
        float waitTime = 0f;
        bool isLocked;
        GameObject currentTarget;

        public EnemyState State { get { return state; } }
        public bool IsWaiting { get { return Time.time <= waitTime; } }
        public bool IsLocked { get { return isLocked; } }

        const float targetFoundTime = 1f;
        const float targetLostTime = 4.13f;

        [Header("GENERAL")]
        [SerializeField] GameObject playerTarget;
        [Space]
        #endregion

        #region MOVEMENT
        NavMeshAgent agent;
        int index = 0;

        [Header("MOVEMENT")]
        [SerializeField] float walkSpeed = 30;
        [SerializeField] float runSpeed = 60;
        [SerializeField] PatrolRoute patrolRoute;
        [SerializeField] bool patrolCycle;
        [SerializeField] Boundaries movementBounds;
        [Space]            
        #endregion

        #region ROTATION
        float progress;
        Quaternion targetRotation;
        Quaternion minRotation;
        Quaternion maxRotation;
        bool hasLooked;

        public float Progress { get { return progress; } }
        public Quaternion TargetRotation { get { return targetRotation; } }

        [Header("ROTATION")]
        [Range(-90f, 90f)] [SerializeField] float rotationMinAngle = 60f;
        [Range(-90f, 90f)] [SerializeField] float rotationMaxAngle = -60f;
        [SerializeField] float rotationSpeed = .1f;    
        [Space]
        #endregion

        #region BEHAVIOUR
        [Header("SIGHT")]
        [SerializeField] GameObject sightPivot;
        [SerializeField] LayerMask wallMask;
        [SerializeField] float maxSightDistance;
        [SerializeField] float maxSightAngle;
        [Space]

        [Header("HEARING")]
        [SerializeField] Boundaries hearingBounds;
        [Space]    

        [Header("VOICE")]
        [SerializeField] AudioClip ahaClip;    
        #endregion

        void Start()
        {
            coll = GetComponent<BoxCollider>();

            agent = GetComponent<NavMeshAgent>();
            agent.speed = walkSpeed;
            
            minRotation = Quaternion.Euler(0f, rotationMinAngle, 0f);
            maxRotation = Quaternion.Euler(0f, rotationMaxAngle, 0f);
            targetRotation = minRotation;
        }

        public void Pursue()
        {
            Move(playerTarget.transform.position);

            if (!TargetInMovementBounds()) {
                TargetLost();
            }
            // TODO        
            // if (agent.remainingDistance <= agent.stoppingDistance) {
                // wait Betinho
            // }
        }

        public void Patrol()
        {     
            if (!agent.hasPath)
            {
                if (patrolRoute.Points[index].lookAround && !hasLooked)
                {
                    state = EnemyState.Searching;
                    transform.rotation = Quaternion.Euler(0f, patrolRoute.Points[index].initialRotation, 0f);
                    return;
                }
                else
                {
                    hasLooked = false;

                    index++;
                    if (index >= patrolRoute.Points.Length)
                    {
                        index = 0;

                        if (!patrolCycle)
                        {
                            Array.Reverse(patrolRoute.Points);
                        }
                    }
                }
            }
            else {
                Move(patrolRoute.Points[index].transform.position);
            }
        }

        private void Move(Vector3 targetPosition)
        {
            agent.isStopped = false;
            agent.ResetPath();
            agent.SetDestination(NormalizeYAxis(targetPosition));        
        }

        public void Search() {
            //TODO -> limite de rotação por tempo
            Rotate();
        }

        void Rotate()
        {
            Quaternion currentRotation = sightPivot.transform.localRotation;        

            //change rotation direction depending on angle
            if (currentRotation == minRotation)
            {
                targetRotation = maxRotation;
                progress = 0;
            }
            else if (currentRotation == maxRotation)
            {
                targetRotation = minRotation;
                progress = 0;
            }

            //apply rotation slerp based on progress to achieve smoothness
            if (progress < 1 && progress >= 0)
            {
                progress += rotationSpeed * Time.deltaTime;
                sightPivot.transform.localRotation = Quaternion.Slerp(currentRotation, targetRotation, progress);
            }
        }

        public void See()
        {
            RaycastHit hit;

            Transform transform = sightPivot.transform;

            //debug
            Vector3 direction1 = Quaternion.AngleAxis(maxSightAngle, transform.up) * transform.TransformDirection(Vector3.forward);
            Vector3 direction2 = Quaternion.AngleAxis(-maxSightAngle, transform.up) * transform.TransformDirection(Vector3.forward);
            Debug.DrawRay(transform.position, direction1 * maxSightDistance, Color.red);
            Debug.DrawRay(transform.position, direction2 * maxSightDistance, Color.red);

            Vector3 pos = new Vector3(transform.position.x, transform.position.y, transform.position.z);

            if (Physics.Raycast(pos, playerTarget.transform.position - pos, out hit, maxSightDistance))
            {
                if (Vector3.Distance(playerTarget.transform.position, pos) > maxSightDistance)
                {
                    //when Player is far from Ignacio's sight but inside his area
                    UIManager.instance.ChangeRadarState(Color.green);
                }
                else if (hit.collider.gameObject.layer == (int)Mathf.Log(wallMask.value, 2))
                {
                    //when Player can be seen by Ignacio, but a wall stands between them
                    UIManager.instance.ChangeRadarState(Color.gray);
                }
                else
                {
                    //when Player can be seen by Ignacio and there is nothing in the way
                    UIManager.instance.ChangeRadarState(Color.yellow);

                    float distance = hit.distance;
                    Vector3 targetDirection = playerTarget.transform.position - pos;

                    float angle = Vector3.Angle(transform.TransformDirection(Vector3.forward), targetDirection);

                    if (angle < maxSightAngle && hit.distance < maxSightDistance)
                    {
                        //Enemy found the Player oh bollocks
                        UIManager.instance.ChangeRadarState(Color.red);
                        TargetFound(playerTarget);
                    }
                }
            }
        }

        public void Hear(GameObject go)
        {
            //TODO -> Verify bomb hearing
            if (currentTarget == null || currentTarget.tag != "Player") {
            TargetFound(go);
            }
        }

        void TargetFound(GameObject go)
        {
            agent.velocity = Vector3.zero;
            agent.isStopped = true;
            currentTarget = go;
            agent.speed = runSpeed;
            state = EnemyState.Alert;
            transform.LookAt(playerTarget.transform.position);
            sightPivot.transform.localRotation = Quaternion.Euler(Vector3.zero);
            Wait(targetFoundTime);
            AudioManager.instance.PlayClip(ahaClip);
        }

        void TargetLost()
        {
            agent.velocity = Vector3.zero;
            agent.isStopped = true;
            currentTarget = null;        
            agent.speed = walkSpeed;
            state = EnemyState.Unaware;            
            Wait(targetLostTime);
        }

        void OnTriggerEnter(Collider coll)
        {
            if (coll.gameObject.tag == "Player")
            {
                playerTarget.SendMessage("PlayerDeath");
            }
        }

        public bool TargetInMovementBounds() {
            Bounds targetBounds = playerTarget.GetComponent<Collider>().bounds;
            bool targetInbounds = movementBounds.Bounds.Intersects(targetBounds);
            return targetInbounds;
        }

        public bool TargetInHearingBounds(GameObject other) {
            Bounds otherBounds;
            if (other.GetComponent<Collider>() != null) {
                otherBounds = other.GetComponent<Collider>().bounds;
            }
            else {
                otherBounds = new Bounds(other.transform.position, new Vector3(coll.bounds.size.x, coll.bounds.size.y, coll.bounds.size.z));
            }
            hearingBounds.Center = transform.position;
            bool targetInbounds = hearingBounds.Bounds.Intersects(otherBounds);
            return targetInbounds;
        } 

        public void Wait(float seconds)
        {
            waitTime = Time.time + seconds;
        }

        public void Lock(bool _lock)
        {
            isLocked = _lock;
        }

        Vector3 NormalizeYAxis(Vector3 vector)
        {
            return new Vector3(vector.x, transform.position.y, vector.z);
        }
    }
}