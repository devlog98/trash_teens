﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public enum DialogueBox { Neutral, Angry, Thought }

[System.Serializable]
public class DialogueBoxData {
    [SerializeField] DialogueBox type;
    [SerializeField] TextMeshProUGUI nameUI;
    [SerializeField] TextMeshProUGUI sentenceUI;
    [SerializeField] Image boxUI;

    public DialogueBox Type { get { return type; } }
    public TextMeshProUGUI NameUI {
        get { return nameUI; }
        set { nameUI = value; }
    }
    public TextMeshProUGUI SentenceUI {
        get { return sentenceUI; }
        set { sentenceUI = value; }
    }
    public Image BoxUI { get { return boxUI; } }
}
