﻿using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[Serializable]
public class Dialogue : PlayableBehaviour {
    public DialogueActor actor;
    public string key;
    public DialogueBox bubble;

    public bool hasToPause;

    bool clipPlayed, clipFinished;
    PlayableDirector director;

    double targetTime;

    public override void OnPlayableCreate(Playable playable) {
        director = (playable.GetGraph().GetResolver() as PlayableDirector);
    }

    public override void ProcessFrame(Playable playable, FrameData info, object playerData) {
        if (!clipFinished) {
            if (!clipPlayed) {
                if (info.weight > 0f) {
                    UIManager.instance.StartDialogue(actor, key, bubble);
                    clipPlayed = true;
                    
                    targetTime = director.time + playable.GetDuration();
                    //Debug.Log(targetTime);
                }
            } else {
                //Debug.Log("Target Time: " + targetTime + " | Director Current Time: " + director.time + info.deltaTime);
                if (targetTime < director.time + info.deltaTime) {
                    if (hasToPause) {
                        TimelineManager.instance.PauseTimeline();
                    } else {
                        UIManager.instance.EndDialogue();
                    }

                    clipFinished = true;
                    director.time = targetTime;
                }
            }
        }
    }
}