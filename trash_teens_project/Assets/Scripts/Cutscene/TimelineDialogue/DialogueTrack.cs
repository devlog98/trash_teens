﻿using UnityEngine;
using UnityEngine.Timeline;
using UnityEngine.Playables;

[TrackColor(0.855f, 0.203f, 0.87f)]
[TrackClipType(typeof(DialogueClip))]
[TrackClipType(typeof(FillClip))]
public class DialogueTrack : TrackAsset { }