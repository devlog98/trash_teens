﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[Serializable]
public class FillClip : PlayableAsset, ITimelineClipAsset {
    public Fill fill = new Fill();

    public ClipCaps clipCaps {
        get { return ClipCaps.None; }
    }

    public override Playable CreatePlayable(PlayableGraph graph, GameObject owner) {
        var playable = ScriptPlayable<Fill>.Create(graph, fill);

        return playable;
    }
}