﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

[Serializable]
public class Fill : PlayableBehaviour {
    bool clipPlayed, clipFinished;
    PlayableDirector director;

    double targetTime;

    public override void OnPlayableCreate(Playable playable) {
        director = (playable.GetGraph().GetResolver() as PlayableDirector);
    }

    public override void ProcessFrame(Playable playable, FrameData info, object playerData) {
        if (!clipFinished) {
            if (!clipPlayed) {
                if (info.weight > 0f) {
                    clipPlayed = true;
                    targetTime = director.time + playable.GetDuration();
                }
            } else {
                if (targetTime < director.time + info.deltaTime) {
                    clipFinished = true;
                    director.time = targetTime;
                }
            }
        }
    }
}