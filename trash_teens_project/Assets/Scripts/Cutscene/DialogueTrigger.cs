﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[RequireComponent(typeof(BoxCollider))]
public class DialogueTrigger : MonoBehaviour {
    [SerializeField] TimelineAsset[] timelines, cageTimelines;
    [SerializeField] SpriteRenderer buttonPrompt;
    [SerializeField] bool playOnTriggerEnter, destroyOnPlay;

    [Header("Assign to save progress of the Player")]
    [SerializeField] LevelComplete levelEnd;

    PlayerModel player;
    bool isPlaying, cageUnlocked;
    int index = 0;

    void Start() {
        if (buttonPrompt) {
            buttonPrompt.gameObject.SetActive(false);
        }

        player = FindObjectOfType<PlayerModel>();
    }

    void TriggerDialogue() {
        TimelineAsset timeline;

        if (player.carryScript.IsCarrying || cageUnlocked) {
            if (!cageUnlocked) {
                levelEnd.StoreLevelProgress();
                cageUnlocked = true;
                index = 0;
            }
            timeline = cageTimelines[index];

            if (index < cageTimelines.Length - 1) {
                index++;
            }
        }
        else {
            timeline = timelines[index];

            if (index < timelines.Length - 1) {
                index++;
            }
        }

        isPlaying = true;
        TimelineManager.instance.PlayTimeline(timeline, TriggerCallback);
        player.LockPlayer();
    }

    #region CALLBACKS
    void TriggerCallback(float waitTime) {
        StartCoroutine(Wait(waitTime));
    }

    IEnumerator Wait(float waitTime) {
        float time = Time.time + waitTime;

        while (Time.time < time) {
            yield return null;
        }

        isPlaying = false;
        player.UnlockPlayer();

        if (destroyOnPlay) {
            Destroy(this.gameObject);
        }
    }
    #endregion

    #region TRIGGERS
    void OnTriggerEnter(Collider other) {
        if (other.gameObject.tag == player.tag) {
            if (playOnTriggerEnter) {
                TriggerDialogue();
            }
            else {
                buttonPrompt.gameObject.SetActive(true);
            }
        }
    }

    void OnTriggerStay(Collider other) {
        if (other.gameObject.tag == player.tag && player.InteractInput && !isPlaying) {
            TriggerDialogue();
        }
    }

    void OnTriggerExit(Collider other) {
        if (other.gameObject.tag == player.tag) {
            buttonPrompt.gameObject.SetActive(false);
        }
    }
    #endregion
}