﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[RequireComponent(typeof(PlayableDirector))]
public class TimelineManager : MonoBehaviour {
    public static TimelineManager instance; //static instance can be called any time

    PlayableDirector director;

    //Playable dialogueClip;
    //public Playable DialogueClip { get { return dialogueClip; } set { dialogueClip = value; } }

    Action<float> callback;
    bool isPaused;

    void Awake() {
        if (instance != null && instance != this) {
            Destroy(this.gameObject);
        }
        else {
            instance = this;
        }
    }

    void OnEnable() {
        director = GetComponent<PlayableDirector>();
        director.stopped += EndTimeline;
    }
    void OnDisable() {
        director.stopped -= EndTimeline;
    }

    void Start() {
        director.playOnAwake = false;
    }

    public void PlayTimeline(TimelineAsset _timeline, Action<float> _callback) {
        if (director.state != PlayState.Playing && !isPaused) {
            callback = _callback;

            director.playableAsset = _timeline;
            director.Play();

            Debug.Log(director.playableAsset.name + " started!");
        }
    }

    public void PauseTimeline() {
        Debug.Log(director.playableAsset.name + " paused!");

        isPaused = true;
        director.Pause();
    }

    public void ResumeTimeline() {
        if (isPaused) {
            Debug.Log(director.playableAsset.name + " resumed!");

            isPaused = false;
            UIManager.instance.EndDialogue();
            director.Resume();            
        }
    }

    void EndTimeline(PlayableDirector _director) {
        Debug.Log(director.playableAsset.name + " ended!");

        if (callback != null) {
            callback(.1f);
        }

        callback = null;
    }
}
