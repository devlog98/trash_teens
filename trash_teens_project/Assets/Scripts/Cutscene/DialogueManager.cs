﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[System.Serializable]
public class Sentences
{
    public string sentence;

    // Changing Sprite
    [Header("Character Changing")]
    public string name;
    public Sprite rightSprite;
    public Sprite leftSprite;
    public Sprite centerSprite;
    public Sprite sentenceBox;

    // Sentence Color
    public Color sentenceColor = new Color(0, 0, 0, 100);

    [Header("Activation Triggers")]
    public GameObject[] objectsToActivate;

    // Sprite animations
    public enum animationTrigger { Popup, MoveFront, MoveBack, Popout, None };
    [Header("Animations")]
    public animationTrigger animTrigger_R;
    public animationTrigger animTrigger_L;
    public Transform playerPos;


    // Camera Movement
    [Header("Camera Angles")]
    public bool cinematicCamera;
    public Transform camPos;

    // Darken screen
    public bool darken;

    [Header("ShakeScreen")]
    public bool DoShake = false;
    public float shakeAmt = 0, shakeLength = 0;

    [Header("Play Sound")]
    public AudioClip sound;
    public AudioSource charVoice;

    [Header("Music Control")]
    public bool muteMusic;
    public AudioClip changeMusic;

    [Header("Cinematic Timing")]
    public bool isTimed = false;
    public float timeToWait;
    public GameObject modelToAnimate;
    public string modelsTrigger;

}

public class DialogueManager : MonoBehaviour
{
    public TextMeshProUGUI textDisplay;
    public TextMeshProUGUI name;

    public PlayerModel playerScript;

    public bool triggeredByButton = false;
    public bool needsCage;
    [SerializeField] SpriteRenderer buttonPrompt;
    [SerializeField] SpriteRenderer cagePrompt;
    

    // Component references
    [Header ("Component References")]

    public Image imgRenderer_R;
    public Image imgRenderer_L;
    public Image imgRenderer_Center;
    public Animator spriteAnim_R;
    public Animator spriteAnim_L;
    public AudioSource musicSource;
    AudioSource audioSource;
    public Image dialogueBox;
    public GameObject darkPanel;

    public GameObject cutsceneHolder;

    public Camera cutsceneCam;
    public Camera mainCam;

    [HideInInspector]
    public int sentenceNumber = 0;

    public float typeSpeed = 0.02f;

    public Sentences[] sentenceArray;

    public bool dialogueActive = false;
    bool cutsceneSeen = false;

    bool textTyped;

    float cinematicWaitTime = 0;

    private void Start()
    {
        textDisplay.text = "";
        //musicSource = GameObject.Find("Music Manager").GetComponent<AudioSource>();
        audioSource = GetComponent<AudioSource>();
    }

    public void ActivateDialogue()
    {
        if(playerScript.isLocked)
        {
            playerScript.UnlockPlayer();
        }
        else
        {
            playerScript.LockPlayer();
        }

        dialogueActive = !dialogueActive;
        cutsceneHolder.SetActive(!cutsceneHolder.activeSelf);
        mainCam.enabled = true;
        sentenceNumber = 0;
        cutsceneSeen = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player" && !cutsceneSeen && !triggeredByButton)
        {
            ActivateDialogue();
            // Start question
            StartCoroutine(typeSentence());
            other.GetComponent<PlayerModel>().dialogueScript = this.GetComponent<DialogueManager>();
        }
    }

    private void OnTriggerStay(Collider other)
    {
        #region triggando dialogo com botão
        // se dialogo for triggado por botão
        if(other.gameObject.tag == "Player" && triggeredByButton)
        {
            // se é necessario ter a jaula para triggar o diálogo
            if (needsCage)
            {
                // se você está carregando a jaula
                if (other.gameObject.GetComponent<PlayerModel>().carryScript.IsCarrying)
                {
                    // mostrar prompt
                    buttonPrompt.gameObject.SetActive(true);

                    // permitir que o dialogo seja ativado
                    if (InputManager.InteractButton(false))
                    {
                        ActivateDialogue();
                        StartCoroutine(typeSentence());
                        other.GetComponent<PlayerModel>().dialogueScript = this.GetComponent<DialogueManager>();
                    }
                }
                // senão, pedir jaula
                else
                    cagePrompt.gameObject.SetActive(true);
            }
            // se não precisa da jaula, só mostra o prompt
            else
            {
                buttonPrompt.gameObject.SetActive(true);
                // e permita que o dialogo seja triggado
                if (InputManager.InteractButton(false))
                {
                    ActivateDialogue();
                    StartCoroutine(typeSentence());
                    other.GetComponent<PlayerModel>().dialogueScript = this.GetComponent<DialogueManager>();
                }
            }
        }
        #endregion
    }
    private void OnTriggerExit(Collider other)
    {
        // desativar prompts
        if(other.gameObject.tag == "Player")
        {
            buttonPrompt.gameObject.SetActive(false);
            cagePrompt.gameObject.SetActive(false);
        }
    }

    private void Update()
    {
        if (buttonPrompt != null)
        { 
            if (buttonPrompt.gameObject.activeInHierarchy)
            {
                cagePrompt.gameObject.SetActive(false);
            }
        }

        if (cagePrompt != null)
        {
            if (cagePrompt.gameObject.activeInHierarchy && cagePrompt.gameObject != null)
            {
                buttonPrompt.gameObject.SetActive(false);
            }
        }


        if (dialogueActive)
        {
            playerScript.moveSettings.verticalVelocity = 0;
            playerScript.LockPlayer();
            playerScript.moveSettings.curMoveSpeed = 0;
            // If the text is done showing, you can call the next one
            if (!sentenceArray[sentenceNumber].isTimed)
            {
                if (textTyped)
                {
                    // When player press button, if there is still sentences left in the dialogue, keep writing them.
                    if (Input.GetButtonDown("Jump") || Input.GetKeyDown(KeyCode.Mouse0) || Input.GetKeyDown(KeyCode.Mouse1))
                    {
                        if (sentenceNumber < sentenceArray.Length - 1)
                        {
                            // stop writing current sentence and move to next one
                            StopCoroutine(typeSentence());

                            textDisplay.text = "";

                            sentenceNumber++;
                            StartCoroutine(typeSentence());
                        }
                        // If all sentences in current dialogue have been said
                        else
                        {
                            textDisplay.text = "";
                            ActivateDialogue();
                            StopCoroutine(typeSentence());
                            playerScript.UnlockPlayer();
                        }
                    }
                }
                // If you want to skip to the end of the sentence instead of waiting on it
                else
                {
                    if (Input.GetButtonDown("Jump") || Input.GetKeyDown(KeyCode.Mouse0) || Input.GetKeyDown(KeyCode.Mouse1))
                    {
                        textTyped = true;
                        StopCoroutine(typeSentence());
                        textDisplay.text = "";
                        textDisplay.text = LocalizationManager.instance.GetLocalizedValue(sentenceArray[sentenceNumber].sentence);
                    }
                }
            }
            else
            {
                if(cinematicWaitTime <= sentenceArray[sentenceNumber].timeToWait)
                {
                    cinematicWaitTime += Time.deltaTime;
                }
                else
                {
                    cinematicWaitTime = 0;
                    if (sentenceNumber < sentenceArray.Length - 1)
                    {
                        StopCoroutine(typeSentence());

                        textDisplay.text = "";

                        sentenceNumber++;
                        StartCoroutine(typeSentence());
                    }
                    else
                    {
                        textDisplay.text = "";
                        ActivateDialogue();
                        StopCoroutine(typeSentence());
                    }
                }
            }
        }
    }

    // Type the client question slowly
    IEnumerator typeSentence()
    {
        textTyped = false;

        //positioning player (if need be)
        if (sentenceArray[sentenceNumber].playerPos != null && playerScript.gameObject.transform != sentenceArray[sentenceNumber].playerPos)
        {
            playerScript.gameObject.transform.position = sentenceArray[sentenceNumber].playerPos.position;
        }

        // Activating objects
        for(int i = 0; i < sentenceArray[sentenceNumber].objectsToActivate.Length; i++)
        {
            sentenceArray[sentenceNumber].objectsToActivate[i].SendMessage("Activate", 0f);
        }

        // Changing sentence box
        if (sentenceArray[sentenceNumber].sentenceBox != null)
        {
            dialogueBox.enabled = true;
            dialogueBox.sprite = sentenceArray[sentenceNumber].sentenceBox;
        }
        else
        {
            dialogueBox.enabled = false;
        }

        // change sprite animation, if its not "None"
        // RIGHT SPRITE
        if (sentenceArray[sentenceNumber].animTrigger_R.ToString() != "None")
        {
            // play animation
            spriteAnim_R.SetTrigger(sentenceArray[sentenceNumber].animTrigger_R.ToString());
        }
        // LEFT SPRITE
        if (sentenceArray[sentenceNumber].animTrigger_L.ToString() != "None")
        {
            // play animation
            spriteAnim_L.SetTrigger(sentenceArray[sentenceNumber].animTrigger_L.ToString());
        }


        // If you have a sprite change
        // RIGHT SPRITE
        if (sentenceArray[sentenceNumber].rightSprite != null)
        {
            imgRenderer_R.enabled = true;
            imgRenderer_R.sprite = sentenceArray[sentenceNumber].rightSprite;
        }
        else
        {
            imgRenderer_R.enabled = false;
        }
        // LEFT SPRITE
        if (sentenceArray[sentenceNumber].leftSprite != null)
        {
            imgRenderer_L.enabled = true;
            imgRenderer_L.sprite = sentenceArray[sentenceNumber].leftSprite;
        }
        else
        {
            imgRenderer_L.enabled = false;
        }
        //CENTER SPRITE

        if (sentenceArray[sentenceNumber].centerSprite != null)
        {
            imgRenderer_Center.gameObject.SetActive(true);
            imgRenderer_Center.sprite = sentenceArray[sentenceNumber].centerSprite;
        }
        else
        {
            imgRenderer_Center.gameObject.SetActive(false);
        }

        //ANIMATE MODELS (only one at a time for now)
        if(sentenceArray[sentenceNumber].modelToAnimate != null)
        {
            Animator modelAnim = sentenceArray[sentenceNumber].modelToAnimate.GetComponent<Animator>();
            modelAnim.SetTrigger(sentenceArray[sentenceNumber].modelsTrigger);
            Debug.Log("++++ TRIGGER THAT YOU SHOULD TRIGGER: " + sentenceArray[sentenceNumber].modelsTrigger + "from: " + modelAnim.gameObject);
        }

        // Darken Screen
            if (sentenceArray[sentenceNumber].darken)
        {
            darkPanel.gameObject.SetActive(true);
        }
        else
        {
            darkPanel.gameObject.SetActive(false);
        }

        // Changing character name
        name.text = sentenceArray[sentenceNumber].name;

        // Change sentence color
        textDisplay.color = sentenceArray[sentenceNumber].sentenceColor;
        sentenceArray[sentenceNumber].sentenceColor.a = 100;

        // Shake screen, if necessary -- could be changed to "Shake Sprite?"
        if (sentenceArray[sentenceNumber].DoShake)
        {
            //camShakeScript.Shake(questionArray[questionNumber].sentences[questionArray[questionNumber].index].shakeAmt, questionArray[questionNumber].sentences[questionArray[questionNumber].index].shakeLength);
        }

        // Camera Angles
        if (sentenceArray[sentenceNumber].cinematicCamera)
        {
            mainCam.enabled = false;
            cutsceneCam.transform.position = sentenceArray[sentenceNumber].camPos.position;
            cutsceneCam.transform.rotation = sentenceArray[sentenceNumber].camPos.rotation;
        }
        // Regular game Cam
        else
        {
            mainCam.enabled = true;
        }

        // play AudioClip 
        if (sentenceArray[sentenceNumber].sound != null)
        {
            audioSource.clip = sentenceArray[sentenceNumber].sound;
            audioSource.Play();
        }

        //mute or unmute music
        //musicSource.mute = sentenceArray[sentenceNumber].muteMusic;

        //change music
        if (sentenceArray[sentenceNumber].changeMusic != null)
        {
            //musicSource.clip = sentenceArray[sentenceNumber].changeMusic;
            //musicSource.Play();
        }

        int soundPlayerIndex = 0;
        // write sentence letter by letter
        string localizedtext = LocalizationManager.instance.GetLocalizedValue(sentenceArray[sentenceNumber].sentence);
        Debug.Log(localizedtext);
        if (localizedtext != null)
        {
            foreach (char letter in localizedtext.ToCharArray())
            {
                if (textTyped == false)
                {
                    textDisplay.text += letter;

                    if (soundPlayerIndex == 5)
                    {
                        //sentenceArray[sentenceNumber].charVoice.Play();
                        soundPlayerIndex = 0;
                    }
                    else
                    {
                        soundPlayerIndex++;
                    }
                    yield return new WaitForSeconds(typeSpeed);
                }
            }
        }
        else
        {
            textDisplay.text = sentenceArray[sentenceNumber].sentence;
        }

        textTyped = true;
    }
}
