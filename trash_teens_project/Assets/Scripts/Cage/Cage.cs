﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MSuits.Carryable {
    public class Cage : MonoBehaviour {
        [SerializeField] PlayerModel player; //reference to PlayerModel
        [SerializeField] ParticleSystem fallSmoke; //smoke effect when cage falls on the ground
        [SerializeField] AudioClip clangSound; //sound effect for cage landing when thrown
        Rigidbody rb;
        
        [Space]

        [SerializeField] int frameInterval = 15; //optimizes Update method

        bool resetCage;

        private void Update() {
            if (Time.frameCount % frameInterval == 0) {
                //reset if Cage is falling (resetCage avoids PlayerDeath method to be called multiple times)
                if (!resetCage && transform.position.y <= -100) {
                    Debug.Log("CAGE FALLEN, KILL ME NOW");
                    player.StartCoroutine("PlayerDeath");
                    resetCage = true;
                }
                else {
                    resetCage = false;
                }
            }
        }

        public void StopFallVelocity() {
            //lazy load rigidbody
            if (rb == null) {
                rb = GetComponent<Rigidbody>();
            }

            //stop velocity
            rb.velocity = Vector3.zero;
        }

        //TODO -> Implement Cage Sound Effects
        void OnCollisionEnter(Collision collision) {
            if (collision.gameObject.tag != "Player" && collision.gameObject.name != "GrabbingHitbox") {
                int pointIndex = Mathf.RoundToInt(collision.contacts.Length * 0.5f);
                fallSmoke.transform.position = collision.GetContact(pointIndex).point;
                fallSmoke.Play();
            }
            
           //AudioManager.instance.MakeNoise(clangSound, this.gameObject);
        }
    }
}