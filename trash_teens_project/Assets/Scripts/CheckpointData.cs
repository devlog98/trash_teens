﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckpointData : MonoBehaviour {

    public static CheckpointData checkpointControl;

    public Vector3 playerPos;
    public Vector3 cagePos;

    public bool isActive;
    public bool isDebug;

    // Use this for initialization
    void Awake() {
        if (checkpointControl == null) {
            DontDestroyOnLoad(gameObject);
            checkpointControl = this;
        } else if (checkpointControl != this) {
            Destroy(gameObject);
        }
    }
}
