﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuButtons : MonoBehaviour
{
    public Animator anim;
    public GameObject pauseMenu;

    // Resume Button
    public void ResumeGame()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        pauseMenu.SetActive(false);
        Time.timeScale = 1.2f;
    }

    // Restart Button
    public void RestartLevel()
    {
        Time.timeScale = 1.2f;
        Debug.Log("Restarted Level" + SceneManager.GetActiveScene().buildIndex);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    // Bestiary Button
    public void BestiaryButton()
    {
        anim.SetTrigger("Bestiary");
    }

    // Bestiary Button
    public void OptionsButton()
    {
        anim.SetTrigger("Options");
    }

    public void ExitLevel()
    {
        Time.timeScale = 1.2f;
        pauseMenu.SetActive(false);
        ProgressionManager.instance.SaveProgress();
        LoadManager.instance.TransitionToScene(1);
    }

    public void BackToMenu()
    {
        ProgressionManager.instance.SaveProgress();
        anim.SetTrigger("BackToMenu");
    }

    public void MainMenu()
    {
        ProgressionManager.instance.SaveProgress();
        SceneManager.LoadScene(0);
    }
}