﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraCollision : MonoBehaviour {

    public LayerMask collisionLayer; //What the camera can collide with

    [HideInInspector]
    public bool colliding = false; // Boolean that checks if it is colliding
    [HideInInspector]
    public Vector3[] adjustedCameraClipPoints; // Clip points surounding camera position
    [HideInInspector]
    public Vector3[] desiredCameraClipPoints; // Clip points surrounding camera's expected position

    Camera camera;

    // Method that initializes Variables
    public void Initialize(Camera cam)
    {
        camera = cam; // Setup the reference to the Camera
        adjustedCameraClipPoints = new Vector3[5]; // This Array will contain the four Clip Points + the Camera Pos
        desiredCameraClipPoints = new Vector3[5]; // This Array will contain the four Desired Clip Points + the Camera Pos
    }

    // Method that finds the Clip Points and adds them into an Array
    public void UpdateCameraClipPoints(Vector3 cameraPosition, Quaternion atRotation, ref Vector3[] intoArray)
    {
        // If we dont have a camera, do nothing
        if (!camera)
            return;

        //Clear the content of the intoArray (Array which the new clip points will be passed into)
        intoArray = new Vector3[5];

        // Getting X, Y, and Z Coordinates of the Clip Points in relation to the Camera position
        float z = camera.nearClipPlane;
        float x = Mathf.Tan(camera.fieldOfView / 3.41f) * z;
        float y = x / camera.aspect;

        //Finding clip points for the  clip points, and assign them to the intoArray

        //Top Left
        intoArray[0] = (atRotation * new Vector3(-x, y, z)) + cameraPosition; // Added and Rotated the Top Left Clip Point, Relative to CameraPos
                                                                              //Top Right
        intoArray[1] = (atRotation * new Vector3(x, y, z)) + cameraPosition; // Added and Rotated the Top Right Clip Point, Relative to CameraPos
                                                                             //Bottom Left
        intoArray[2] = (atRotation * new Vector3(-x, -y, z)) + cameraPosition; // Added and Rotated the Bottom Left Clip Point, Relative to CameraPos
                                                                               //Bottom Right
        intoArray[3] = (atRotation * new Vector3(x, -y, z)) + cameraPosition; // Added and Rotated the Bottom Right Clip Point, Relative to CameraPos
                                                                              // Camera Position
        intoArray[4] = cameraPosition - camera.transform.forward; // -camera.transform.forward, giving a little bit of room behind the camera to collide with
    }

    // Method that detects if collisions happened at our clip points
    bool CollisionDetectedAtClipPoints(Vector3[] clipPoints, Vector3 fromPosition)
    {
        for (int i = 0; i < clipPoints.Length; i++) // Looping through the array of clip points
        {
            Ray ray = new Ray(fromPosition, clipPoints[i] - fromPosition); // Casting a Ray from the player, to the clip points
            float distance = Vector3.Distance(clipPoints[i], fromPosition); // Calculating the distance from the player to the clip points

            if (Physics.Raycast(ray, distance, collisionLayer)) //If we cast a ray(var1), and through this distance(var2), we collide with something(var3)
            {
                return true; // A collision happened!!
            }
        }

        return false; //No collisions happening;
    }

    // Return a distance that the camera needs to be from our target if a collision happens
    public float getAdjustedDistanceWithRayFrom(Vector3 from)
    {
        float distance = -1;

        for (int i = 0; i < desiredCameraClipPoints.Length; i++) // Loop through the array of desired clip points
        {
            // Another raycast, to find the shortest distance between any of the clip points that are colliding
            Ray ray = new Ray(from, desiredCameraClipPoints[i] - from); // gives us the direction of the target position, towards the desired camera Clip Point
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit)) // we will find where the collision happened with the Raycast hit value
            {
                if (distance == -1) // if distance hasn't been set yet
                    distance = hit.distance; // Set it and give it its initial value
                else
                {
                    if (hit.distance < distance)
                        distance = hit.distance; // Look through all of the collision that we have, and find the shortest distance to have our camera from our target
                }
            }
        }

        if (distance == -1) // If the distance is still -1, we havent collided, and dont need to move our Camera
            return 0;
        else
            return distance;
    }

    // Determining whether or not we are setting or Colliding boolean to true or false
    public void checkColliding(Vector3 targetPosition)
    {
        if (CollisionDetectedAtClipPoints(desiredCameraClipPoints, targetPosition)) // if a collision happened between the clip points and the target
        {
            colliding = true;
        }
        else
        {
            colliding = false;
        }
    }
}
