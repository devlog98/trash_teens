﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivatableObjectRespawnHandler : MonoBehaviour {

    public static ActivatableObjectRespawnHandler objectHandler;

    public bool isActive;

    // Use this for initialization
    void Awake()
    {
        if (objectHandler == null)
        {
            DontDestroyOnLoad(gameObject);
            objectHandler = this;
        }
        else if (objectHandler != this)
        {
            Destroy(gameObject);
        }
    }
}
