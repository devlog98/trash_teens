﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpingHazards : MonoBehaviour
{
    public float timeToJump = 2f;
    float curTime = 0;

    public float jumpHeight = 10f;
    public float gravity = 10f;

    public float maxVerticalVel = 30f;

    float verticalVelocity;

    public float rayDistance = 10f;

    Rigidbody rb;

    public bool isDeadly;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player" && isDeadly)
        {
            Debug.Log("kill kill kill");
           StartCoroutine(collision.gameObject.GetComponent<PlayerModel>().PlayerDeath());
        }
    }

    void Update()
    {
        Move();

        if(IsGrounded())
        {
            if(curTime > timeToJump)
            {
                //Jump
                verticalVelocity = jumpHeight;
                curTime = 0;
            }
            else
            {
                curTime += Time.deltaTime;
            }
        }
        else
        {
            if(verticalVelocity >= -maxVerticalVel)
            verticalVelocity -= gravity * Time.deltaTime;
        }
    }

    public void Move()
    {
        rb.velocity = new Vector3(rb.velocity.x, verticalVelocity, rb.velocity.z);
    }

    bool IsGrounded()
    {
        return Physics.Raycast(transform.position, Vector3.down, rayDistance);
    }
}
