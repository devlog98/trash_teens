﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer {
    float startTime = 0;
    float endTime = 0;

    //starts timer count
    public void StartTimer() {
        startTime = Time.time;
    }

    //ends timer count
    public float StopTimer() {
        endTime = Time.time;
        return endTime - startTime;
    }
}