﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterUnlock : MonoBehaviour {

    [Tooltip("1: Bouncepad, 2: Hole, 3: Bomb, 4: Glue")]
    public int itemToUnlock;

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            switch (itemToUnlock)
            {
                // ------ BOMB ------
                case (3):
                other.gameObject.GetComponent<ItemSwapper>().bombEquipped = true;
                    other.gameObject.GetComponent<ItemSwapper>().curItem = 3;
                    break;
            }
        }
    }
}
