﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateCheckpoint : MonoBehaviour {

    public bool isActivated = false;
    public GameObject light;


    public void Activate()
    {
        isActivated = true;
        light.SetActive(true);
    }
}
