﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectsRespawnHandler : MonoBehaviour {

    public static ObjectsRespawnHandler objectControl;

    public bool isTurnedOn = false;

    void Awake()
    {
        if (objectControl == null)
        {
            DontDestroyOnLoad(gameObject);
            objectControl = this;
        }
        else if (objectControl != this)
        {
            Destroy(gameObject);
        }
    }
}
