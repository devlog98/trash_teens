﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CountdownToActivation  : MonoBehaviour, IActivatable
{
    public float timeToDeactivate;
    [HideInInspector]
    public float curTime = 0;

    public ActivatablePropertie activatableProperty;

    public GameObject[] objectToActivate;
    public GameObject[] buttonThatActivatedThis;

    void Update()
    {
        if(activatableProperty.activated)
        {
            for (int i = 0; i < objectToActivate.Length; i++)
            {
                objectToActivate[i].SendMessage("Activate", 0f);
            }

            if (timeToDeactivate >= curTime)
            {
                curTime += Time.deltaTime;
            }
            else
            {
                for (int i = 0; i < objectToActivate.Length; i++)
                {
                    Debug.Log("Deactivating Objects");
                    objectToActivate[i].SendMessage("Activate", 1f);
                }

                Debug.Log("We are deactivating everything");
                for (int i = 0; i < buttonThatActivatedThis.Length; i++)
                {
                    Debug.Log("Deactivating Buttons");
                    buttonThatActivatedThis[i].SendMessage("Activate", 1f);
                }

                activatableProperty.activated = false;
                curTime = 0;
            }
        }
    }

    public void Activate(float activationParameter)
    {
        if(activationParameter == 0)
        {
            activatableProperty.activated = true;
        }
        else if (activationParameter == 1)
        {
            activatableProperty.activated = false;
        }
        else
        {
            activatableProperty.activated = !activatableProperty.activated;
        }

        curTime = 0;
    }
}
