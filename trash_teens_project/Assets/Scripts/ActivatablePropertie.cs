﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivatablePropertie : MonoBehaviour
{

    public bool activated = false;

    [Tooltip("0: Moving Platform, 1: FireBreather, 2: Rotating Platform, 3: Descending Platform, 4: ExplodableWall, 5: Button")]
    public int activatableType;
}
