﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneHandler : MonoBehaviour {

    public void ReloadScene()
    {
        LoadManager.instance.TransitionToScene(SceneManager.GetActiveScene().buildIndex);
        Time.timeScale = 1f;
    }
}
