﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraLookAt : MonoBehaviour {
    public Transform player;
    public Transform guard;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.LookAt(guard);

        Vector3 distance = guard.position - player.position;
        transform.position = guard.position - distance/6;

        Debug.DrawRay(player.position, distance * 10, Color.blue);
    }
}
