﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider))]
public class SlideTrap : MonoBehaviour
{
    PlayerModel player;

    void Start()
    {
        player = FindObjectOfType<PlayerModel>();
    }

    void OnTriggerStay(Collider coll)
    {
        if (coll.tag == player.tag)
        {
            player.Slide(true);
        }
    }
}