﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//public enum isActive { Activated, Deactivated }
public enum ActivationType { Timed, Button, AlwaysOn }
public static class isActive
{
    public static bool activated;
}

public class FireBreather : MonoBehaviour, IActivatable {
    //public bool activationByButton = false; // If it is activated by Button or by Time
    public ActivationType activationType;
    // Time it stays on and time it stays off
    public float timeToActivate;
    public float timeToDeactivate;
    public float warningTime;

    // Distance the fire can kill you
    public float rayDistance = 10;

    // Particle References
    public ParticleSystem fireParticles;
    public ParticleSystem blockedFireParticles;

    //Current State
    public bool isActivated = false;
    private bool isBeingBlocked = false;

    float elapsedTime = 0;

    Vector3 origin;
    Vector3 direction;
    public LayerMask layermask;

    void Start () {
        origin = transform.position - new Vector3(0,1,0);
        direction = transform.up;

        fireParticles.Stop();

        // Start active if Always On
        if (activationType == ActivationType.AlwaysOn)
            Activate(0);
    }
	
	void Update () {
        // If its timed, and is waiting for activation
        if(isActivated == false && activationType == ActivationType.Timed)
        {
            // Run the timer
            if (elapsedTime <= timeToActivate)
            {
                elapsedTime += Time.deltaTime;
                // throw a warning when getting close
                if(elapsedTime >= timeToActivate - warningTime)
                {
                    blockedFireParticles.gameObject.SetActive(true);
                }
            }
            else
            {
                // Activate
                Activate(0);
                elapsedTime = 0;
                blockedFireParticles.gameObject.SetActive(false);
            }
        }
        // If its timed, and is waiting for deactivation
        else if(isActivated == true && activationType == ActivationType.Timed)
        {
            if(elapsedTime <= timeToDeactivate)
            {
                elapsedTime += Time.deltaTime;
            }
            else
            {
                // Deactivate
                Activate(0);
                elapsedTime = 0;
            }
        }
	}

    private void FixedUpdate()
    {
        CheckForObject();
    }

    private void CheckForObject()
    {
        #region Raycasts
        Ray rayCenter = new Ray(transform.position, transform.up);
        Ray rayLeft = new Ray(new Vector3(transform.position.x + 1.5f, transform.position.y, transform.position.z), transform.up);
        Ray rayRight = new Ray(new Vector3(transform.position.x - 1.5f, transform.position.y, transform.position.z), transform.up);
        Ray rayUp = new Ray(new Vector3(transform.position.x, transform.position.y, transform.position.z - 1.5f), transform.up);
        Ray rayDown = new Ray(new Vector3(transform.position.x, transform.position.y, transform.position.z + 1.5f), transform.up);

        Ray diagonalUpLeft = new Ray(new Vector3(transform.position.x + 1.5f, transform.position.y, transform.position.z + 1.5f), transform.up);
        Ray diagonalUpRight = new Ray(new Vector3(transform.position.x - 1.5f, transform.position.y, transform.position.z + 1.5f), transform.up);
        Ray diagonalDownLeft = new Ray(new Vector3(transform.position.x + 1.5f, transform.position.y, transform.position.z - 1.5f), transform.up);
        Ray diagonalDownRight = new Ray(new Vector3(transform.position.x - 1.5f, transform.position.y, transform.position.z - 1.5f), transform.up);

        Ray SmalldiagonalUpLeft = new Ray(new Vector3(transform.position.x + 1f, transform.position.y, transform.position.z + 1f), transform.up);
        Ray SmalldiagonalUpRight = new Ray(new Vector3(transform.position.x - 1f, transform.position.y, transform.position.z + 1f), transform.up);
        Ray SmalldiagonalDownLeft = new Ray(new Vector3(transform.position.x + 1f, transform.position.y, transform.position.z - 1f), transform.up);
        Ray SmalldiagonalDownRight = new Ray(new Vector3(transform.position.x - 1f, transform.position.y, transform.position.z - 1f), transform.up);

        RaycastHit hit;

        // ------------- SIDE RAYCASTS ------------------
        Debug.DrawLine(transform.position, transform.position + transform.up * rayDistance, Color.green);
        Debug.DrawLine(new Vector3(transform.position.x + 1.5f, transform.position.y, transform.position.z), new Vector3(transform.position.x + 1.5f, transform.position.y, transform.position.z) + transform.up * rayDistance, Color.green);
        Debug.DrawLine(new Vector3(transform.position.x - 1.5f, transform.position.y, transform.position.z), new Vector3(transform.position.x - 1.5f, transform.position.y, transform.position.z) + transform.up * rayDistance, Color.green);
        // --------------------- UP RAYCASTS ------------------
        Debug.DrawLine(new Vector3(transform.position.x, transform.position.y, transform.position.z + 1.5f), new Vector3(transform.position.x, transform.position.y, transform.position.z + 1.5f) + transform.up * rayDistance, Color.green);
        Debug.DrawLine(new Vector3(transform.position.x, transform.position.y, transform.position.z - 1.5f), new Vector3(transform.position.x, transform.position.y, transform.position.z - 1.5f) + transform.up * rayDistance, Color.green);
        // --------------- DIAGONAL RAYCASTS --------------------
        Debug.DrawLine(new Vector3(transform.position.x + 1.5f, transform.position.y, transform.position.z + 1.5f), new Vector3(transform.position.x + 1.5f, transform.position.y, transform.position.z + 1.5f) + transform.up * rayDistance, Color.green);
        Debug.DrawLine(new Vector3(transform.position.x - 1.5f, transform.position.y, transform.position.z + 1.5f), new Vector3(transform.position.x - 1.5f, transform.position.y, transform.position.z + 1.5f) + transform.up * rayDistance, Color.green);
        Debug.DrawLine(new Vector3(transform.position.x + 1.5f, transform.position.y, transform.position.z - 1.5f), new Vector3(transform.position.x + 1.5f, transform.position.y, transform.position.z - 1.5f) + transform.up * rayDistance, Color.green);
        Debug.DrawLine(new Vector3(transform.position.x - 1.5f, transform.position.y, transform.position.z - 1.5f), new Vector3(transform.position.x - 1.5f, transform.position.y, transform.position.z - 1.5f) + transform.up * rayDistance, Color.green);
        // ----------------- SMALLER DIAGONAL RAYCASTS ------------
        Debug.DrawLine(new Vector3(transform.position.x + 0.8f, transform.position.y, transform.position.z + 0.8f), new Vector3(transform.position.x + 0.8f, transform.position.y, transform.position.z + 0.8f) + transform.up * rayDistance, Color.green);
        Debug.DrawLine(new Vector3(transform.position.x - 0.8f, transform.position.y, transform.position.z + 0.8f), new Vector3(transform.position.x - 0.8f, transform.position.y, transform.position.z + 0.8f) + transform.up * rayDistance, Color.green);
        Debug.DrawLine(new Vector3(transform.position.x + 1f, transform.position.y, transform.position.z - 1f), new Vector3(transform.position.x + 1f, transform.position.y, transform.position.z - 1f) + transform.up * rayDistance, Color.green);
        Debug.DrawLine(new Vector3(transform.position.x - 1f, transform.position.y, transform.position.z - 1f), new Vector3(transform.position.x - 1f, transform.position.y, transform.position.z - 1f) + transform.up * rayDistance, Color.green);
        #endregion
        
        // Throw up some rays
        if (Physics.Raycast(rayCenter, out hit, rayDistance, layermask) || Physics.Raycast(rayLeft, out hit, rayDistance, layermask) || Physics.Raycast(rayRight, out hit, rayDistance, layermask) || Physics.Raycast(rayUp, out hit, rayDistance, layermask) || Physics.Raycast(rayDown, out hit, rayDistance, layermask)
            || Physics.Raycast(diagonalDownLeft, out hit, rayDistance, layermask) || Physics.Raycast(diagonalDownRight, out hit, rayDistance, layermask) || Physics.Raycast(diagonalUpLeft, out hit, rayDistance, layermask) || Physics.Raycast(diagonalUpRight, out hit, rayDistance, layermask)
            || Physics.Raycast(SmalldiagonalDownLeft, out hit, rayDistance, layermask) || Physics.Raycast(SmalldiagonalDownRight, out hit, rayDistance, layermask) || Physics.Raycast(SmalldiagonalUpLeft, out hit, rayDistance, layermask) || Physics.Raycast(SmalldiagonalUpRight, out hit, rayDistance, layermask))
        {
            // If it hits walls or carryables, block it
            if (hit.transform.gameObject.tag == "Carryable" || hit.transform.gameObject.layer == 8)
            {
                isBeingBlocked = true;

                if (isActivated)
                {
                    fireParticles.gameObject.SetActive(false);
                    blockedFireParticles.gameObject.SetActive(true);
                }
                else
                {
                    fireParticles.gameObject.SetActive(false);
                    blockedFireParticles.gameObject.SetActive(false);
                }
            }
            // If it doesnt hit walls or carryables, and hits player, kill him
            else if (hit.transform.gameObject.tag == "Player")
            {
                if (isActivated && !isBeingBlocked)
                {
                    hit.transform.gameObject.SendMessage("PlayerDeath");
                }
            }
            // if it hits nothing, or something that's irrelevant, don't block it
            else
            {
                isBeingBlocked = false;
                if (isActivated)
                {
                    blockedFireParticles.gameObject.SetActive(false);
                    fireParticles.gameObject.SetActive(true);
                }
                else
                {
                    blockedFireParticles.gameObject.SetActive(false);
                }
            }
        }
        else
        {
            isBeingBlocked = false;
            if (isActivated)
            {
                blockedFireParticles.gameObject.SetActive(false);
                fireParticles.gameObject.SetActive(true);
            }
            else
            {
                blockedFireParticles.gameObject.SetActive(false);
            }
        }
    }

    public void Activate(float ActivationParameter)
    {
        isActivated = !isActivated;
        fireParticles.gameObject.SetActive(!fireParticles.gameObject.activeInHierarchy);
    }
}