﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectEnabler : MonoBehaviour {
    public GameObject[] objectsToEnable;
    public GameObject[] objectsToDisable;

    public GameObject cage;
    public GameObject player;
    public bool disableCage;

    PlayerModel playerScript;

    public void Activate(float activationParameter) {
        for (int i = 0; i < objectsToEnable.Length; i++) {
            objectsToEnable[i].SetActive(true);
        }

        for (int i = 0; i < objectsToDisable.Length; i++) {
            objectsToDisable[i].SetActive(false);
        }

        if (disableCage) {
            //lazy load playerScript
            if (playerScript == null) {
                playerScript = player.GetComponent<PlayerModel>();
            }

            playerScript.Drop(playerScript.carryDrop.transform.position);
            Destroy(cage.gameObject, 0f);
        }
    }
}
