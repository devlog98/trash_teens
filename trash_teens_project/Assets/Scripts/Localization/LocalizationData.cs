﻿[System.Serializable]
public class LocalizationData {
    public LocalizationScene[] scenes;
}

[System.Serializable]
public class LocalizationScene {
    public string key;
    public LocalizationCutscene[] cutscenes;
}

[System.Serializable]
public class LocalizationCutscene {
    public string key;
    public LocalizationItem[] sentences;
}

[System.Serializable]
public class LocalizationItem {
    public string key;
    public string value;
}