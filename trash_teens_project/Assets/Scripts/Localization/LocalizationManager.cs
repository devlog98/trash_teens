﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class LocalizationManager : MonoBehaviour {
    public static LocalizationManager instance;

    Dictionary<string, string> localizedText;
    string missingValue = "Localized string not found!";
    bool ready;

    public bool Ready { get { return ready; } }

	void Awake () {
        if (instance != null && instance != this) {
            Destroy(this.gameObject);
        }
        else {
            instance = this;
        }

        DontDestroyOnLoad(gameObject);
	}

    //JUST FOR TEST YOU MUST DELET THIS ON FINAL BUILD
    void Start() {
        LoadLocalizedText("localizedText_pt_new.json");
    }

    public void LoadLocalizedText(string fileName) {
        localizedText = new Dictionary<string, string>();
        string filePath = Path.Combine(Application.streamingAssetsPath, fileName);

        if (File.Exists(filePath)) {
            string dataAsJson = File.ReadAllText(filePath);
            LocalizationData loadedData = JsonUtility.FromJson<LocalizationData>(dataAsJson);

            foreach (LocalizationScene scene in loadedData.scenes) {
                foreach (LocalizationCutscene cutscene in scene.cutscenes) {
                    foreach (LocalizationItem item in cutscene.sentences) {
                        localizedText.Add(item.key, item.value);
                    }
                }
            }            

            Debug.Log("Data loaded! Dictionary contains " + localizedText.Count + " entries!");
        }
        else {
            Debug.LogError("Cannot find localization file!!");
        }

        ready = true;
    }

    public string GetLocalizedValue(string key) {        
        string result = missingValue;
        localizedText.TryGetValue(key, out result);        
        return result;
    }
}
