﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartupManager : MonoBehaviour {
	IEnumerator Start () {
        while (!LocalizationManager.instance.Ready) {
            yield return null;
        }

        SceneManager.LoadScene("Dialogue_Test_Area");
	}
}
