﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class LoadLevelInfo : MonoBehaviour {
    TextMeshProUGUI levelName, playtime, completion;
    Animator anim;

    float minutes, seconds;

    void Start() {
        levelName = this.transform.Find("LevelName").GetComponent<TextMeshProUGUI>();
        playtime = this.transform.Find("Playtime").GetComponent<TextMeshProUGUI>();
        completion = this.transform.Find("Completion").GetComponent<TextMeshProUGUI>();

        anim = GetComponent<Animator>();
    }

    public void ShowLevelInfo(ProgressData progress) {
        int cardNumber = progress.completedLevels.Count;
        levelName.text = cardNumber > 0 ? progress.completedLevels[cardNumber - 1].name : "Bem Vindo ao Reino"; //placeholder

        //get nice time
        minutes = Mathf.Floor(progress.totalPlaytime / 60);
        seconds = Mathf.RoundToInt(progress.totalPlaytime % 60);
        playtime.text = string.Format("{0:0}:{1:00}", minutes, seconds);

        completion.text = (((float)cardNumber / 15) * 100).ToString("0.0") + "% Completo";

        anim.SetTrigger("Show");
    }

    public void HideLevelInfo() {
        anim.SetTrigger("Hide");
    }
}