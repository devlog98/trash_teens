﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class LoadFileTemplate : MaskableGraphic, IPointerEnterHandler, IPointerExitHandler {
    ProgressData progress;
    [SerializeField] LoadLevelInfo loadLevelInfo;

    public ProgressData Progress { set { progress = value; } }

    public void OnPointerEnter(PointerEventData eventData) {
        loadLevelInfo.ShowLevelInfo(progress);
    }
    
    public void OnPointerExit(PointerEventData eventData) {
        loadLevelInfo.HideLevelInfo();
    }

    //MAY BE USEFUL IN THE FUTURE
    //ISelectHandler
    //public void OnSelect(BaseEventData eventData) {
    //    Debug.Log("Selected!");
    //}

    //IDeselectHandler
    //public void OnDeselect(BaseEventData eventData) {
    //    Debug.Log("Deselected!");
    //}
}