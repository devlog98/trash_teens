﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour {
    [SerializeField] Animator anim;
    [SerializeField] GameObject loadGameScreen, loadFileContainer, loadFileTemplate;
    [SerializeField] string loadOneCardKey, loadManyCardsKey;

    //NEW GAME STUFF
    public void NewGame() {
        //get all load files and create new file with next index
        int nextIndex = GetAllFileNames().Length;
        ProgressionManager.instance.SaveProgress(SaveSystem.filenameBase + nextIndex);

        //play intro anim
        StartCoroutine(GoingInsideTrashAndLoadingScene());
    }

    IEnumerator GoingInsideTrashAndLoadingScene() {
        anim.SetTrigger("ButtonPress");
        yield return new WaitForSeconds(anim.GetCurrentAnimatorStateInfo(0).length);
        LoadManager.instance.TransitionToScene(2);
    }

    //LOAD GAME STUFF
    public void LoadGame() {
        loadGameScreen.SetActive(true);
        loadFileTemplate.SetActive(false);

        //get all load files
        string[] files = GetAllFileNames();

        //create a load button for each file
        for (int i = 0; i < files.Length; i++) {
            GameObject newFile = null;

            //instantiate new button
            newFile = Instantiate(loadFileTemplate, loadFileContainer.transform);
            newFile.name = "File " + i;

            //change new button values
            newFile.transform.Find("FileName").GetComponent<TextMeshProUGUI>().text = files[i];

            ProgressData progress = SaveSystem.LoadGame(files[i]);
            int cardNumber = progress.completedLevels.Count;

            newFile.transform.Find("CardNumber").GetComponent<TextMeshProUGUI>().text = 
                cardNumber + " " + LocalizationManager.instance.GetLocalizedValue(cardNumber != 1 ? loadManyCardsKey : loadOneCardKey);

            newFile.transform.Find("TrashbinFill").GetComponent<Image>().fillAmount = (float)cardNumber / 15; //número total de cartas (até o momento)

            //set level information values to be changed on hover
            newFile.GetComponent<LoadFileTemplate>().Progress = progress;           

            newFile.SetActive(true);
        }
    }

    public void LoadFromFile(TextMeshProUGUI fileName) {
        ProgressionManager.instance.LoadProgress(fileName.text);
        LoadManager.instance.TransitionToScene(1);
    }

    //OPTIONS STUFF
    public void Options() {
        //Options Here
    }

    //QUIT STUFF 
    public void Quit() {
        Application.Quit();
    }

    //GENERAL
    string[] GetAllFileNames() {
        string[] files = Directory.GetFiles(Application.persistentDataPath, "*.lo");

        for (int i = 0; i < files.Length; i++) {
            files[i] = Path.GetFileNameWithoutExtension(files[i]);
        }

        return files;
    }
}