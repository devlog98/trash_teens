﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public enum DialogueActor { Betinho, Tóti, Alfonso, Ignácio, Ignobô, Punkibara, Unknown }

public class UIManager : MonoBehaviour {
    public static UIManager instance; //static instance can be called any time

    //CHECKPOINT
    [SerializeField] Animator checkpointAnim;
    [SerializeField] AudioClip checkpointSound;

    //DIALOGUE
    [SerializeField] List<DialogueBoxData> dialogueBoxes;
    [Space]
    DialogueBoxData box;

    //RADAR
    [Header("Enemy Radar")]
    [SerializeField] Animator radarAnim;
    [SerializeField] Image radarState;

    void Awake() {
        if (instance != null && instance != this) {
            Destroy(this.gameObject);
        }
        else {
            instance = this;
        }
    }

    void Start() {
        foreach (DialogueBoxData dialogueBox in dialogueBoxes) {
            dialogueBox.NameUI.alpha = 0;
            dialogueBox.SentenceUI.alpha = 0;
            dialogueBox.BoxUI.color = new Color(255, 255, 255, 0);
        }
    }

    public void StartDialogue(DialogueActor actor, string key, DialogueBox bubble) {
        //set current dialogue box
        box = dialogueBoxes.Find(x => x.Type == bubble);
        if (box == null) {
            box = dialogueBoxes[0];
        }

        box.NameUI.alpha = 1;
        box.SentenceUI.alpha = 1;
        box.BoxUI.color = new Color(255, 255, 255, 1);

        //write stuff
        box.NameUI.text = (actor == DialogueActor.Unknown) ? "???" : actor.ToString();

        string localizedValue = LocalizationManager.instance.GetLocalizedValue(key);
        StopAllCoroutines();
        StartCoroutine(TypeDialogue(localizedValue));
    }

    public void EndDialogue() {
        if (box != null) {
            box.NameUI.alpha = 0;
            box.SentenceUI.alpha = 0;
            box.BoxUI.color = new Color(255, 255, 255, 0);
        }
    }

    IEnumerator TypeDialogue(string sentence) {
        box.SentenceUI.text = sentence;
        box.SentenceUI.ForceMeshUpdate(true);

        int totalVisibleCharacters = box.SentenceUI.textInfo.characterCount;
        int counter = 0;

        while (true) {
            int visibleCount = counter % (totalVisibleCharacters + 1);
            box.SentenceUI.maxVisibleCharacters = visibleCount; //how many characters should TMPRO display

            //once the last character has been revealed, end coroutine
            if (visibleCount >= totalVisibleCharacters) {
                break;
            }

            counter++;
            yield return new WaitForSeconds(.001f);
        }
    }

    #region CHECKPOINT
    //play checkpoint animation and sound effect
    public void ActivateCheckpoint() {        
        checkpointAnim.SetTrigger("Checkpoint");
        AudioManager.instance.PlayClip(checkpointSound);
    }
    #endregion

    #region ENEMY VISION RADAR
    public void ActivateRadar(bool active) {
        radarAnim.SetBool("Activation", active);
    }

    public void ChangeRadarState(Color color) {
        radarState.color = color;
    }
    #endregion    
}