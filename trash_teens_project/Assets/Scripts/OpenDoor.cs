﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenDoor : MonoBehaviour {

    public GameObject[] door;
    public bool isToggle;
    public bool isTrigger;
    private int i;

    private void OnCollisionStay(Collision collision)
    {
        if (isTrigger == false)
        {
            if (collision.gameObject.tag == "Player" || collision.gameObject.tag == "Carryable")
            {
                while (i < door.Length)
                {
                    door[i].GetComponent<Animator>().SetBool("ShouldOpen", true);
                    i++;
                }
            }
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        i = 0;
        if (!isToggle)
        {
            while (i < door.Length)
            {
                door[i].GetComponent<Animator>().SetBool("ShouldOpen", false);
                i++;
            }
        }
        i = 0;
    }

    //HACK MONSTRO
    private void OnTriggerStay(Collider collision)
    {
        if (isTrigger)
        {
            if (collision.gameObject.tag == "Player" || collision.gameObject.tag == "Carryable")
            {
                while (i < door.Length)
                {
                    door[i].GetComponent<Animator>().SetBool("ShouldOpen", true);
                    i++;
                }
            }
        }
    }
  
    private void OnTriggerExit(Collider collision)
    {
        i = 0;
        if (!isToggle)
        {
            while (i < door.Length)
            {
                door[i].GetComponent<Animator>().SetBool("ShouldOpen", false);
                i++;
            }
        }
        i = 0;
    }
}
