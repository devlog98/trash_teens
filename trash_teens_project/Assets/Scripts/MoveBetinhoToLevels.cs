﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MoveBetinhoToLevels : MonoBehaviour
{
    public LookAtLevels lookScript;
    NavMeshAgent agent;
    Animator anim;
    

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        anim = GetComponent<Animator>();
        anim.SetBool("isGrounded", true);
    }
    
    void Update()
    {
        agent.destination = new Vector3(lookScript.levels[lookScript.currentLevel].markerPos.position.x, 
                                        lookScript.levels[lookScript.currentLevel].markerPos.position.y, 
                                        lookScript.levels[lookScript.currentLevel].markerPos.position.z);

        if (agent.remainingDistance > 0.15)
        {
            anim.SetInteger("State", 2);
            anim.SetBool("isMoving", true);
        }
        else
        {
            anim.SetInteger("State", 0);
            anim.SetBool("isMoving", false);
            transform.LookAt(lookScript.gameObject.transform);
        }
    }
}
