﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LookAtLevels : MonoBehaviour {
    public Transform target;
    public float smoothSpeed = 0.125f;
    public Vector3 offset;

    public int currentLevel = 0;
    public List<Level> levels = new List<Level>();

    public bool unlockLevelsDebug;

    public bool buttonDown;

    void Start() {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;

        GetLevelData();
    }

    void Update() {
        //limiting joystick movement
        float currentInput = InputManager.HorizontalInput();

        if (currentInput == 0) {
            buttonDown = false;
        }

        target = levels[currentLevel].markerPos;

        if (!buttonDown) {
            if (currentInput < 0) {
                if(currentLevel > 0)
                currentLevel--;

                if (currentLevel <= 0) {
                    currentLevel = 0;
                }

                buttonDown = true;
            }
            if (currentInput > 0)
            {
                if (currentLevel < levels.Count - 1)
                    currentLevel++;

                if (unlockLevelsDebug && currentLevel > levels.Count - 1) {
                    currentLevel = levels.Count - 1;
                }
               // if (currentLevel > ProgressionManager.instance.CompletedLevels.Count)
                //{
                   // currentLevel = ProgressionManager.instance.CompletedLevels.Count;
                //}

                buttonDown = true;
            }
        }        

        // Entering Level
        if (InputManager.InteractButton(false)) {
            LoadManager.instance.TransitionToScene(levels[currentLevel].levelIndex);
        }
    }

    void LateUpdate() {
        // Position Camera at level Marker
        Vector3 desiredPos = target.position + offset;
        Vector3 smoothPos = Vector3.Lerp(transform.position, desiredPos, smoothSpeed * Time.deltaTime);
        transform.position = smoothPos;

        // Look at Level Marker
        //transform.LookAt(target);
    }

    void GetLevelData() {
        foreach (LevelData unlockedLevel in ProgressionManager.instance.CompletedLevels) {
            for (int i = 0; i < levels.Count; i++) {
                if (levels[i].name == unlockedLevel.name) {
                    levels[i].time = unlockedLevel.time;
                }
            }
        }
    }
}

[System.Serializable]
public class Level {
    // Level information
    public string name;
    public float time;
    public int unlockIndex;

    // Navigation
    public Transform markerPos;
    public int levelIndex;

    // Visual
    public GameObject clickPrompt;
}