﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(Collider))]
public class LevelComplete : MonoBehaviour {
    [Header("LOGIC")]
    [SerializeField] PlayerModel player; //reference to PlayerModel script
    [SerializeField] int nextSceneIndex = 1;
    [SerializeField] bool locked; //if level is locked from the beginning or not
    [SerializeField] bool autoFinish; //if level will end automatically after level end is unlocked
    
    [Space]

    [Header("VISUAL")]
    [SerializeField] SpriteRenderer buttonPrompt; //sprite prompts
    [SerializeField] SpriteRenderer cagePrompt; //sprite prompts

    Animator anim;

    void Start() {
        if (buttonPrompt) {
            buttonPrompt.gameObject.SetActive(false);
        }
    }

    void Update() {
        if (buttonPrompt.gameObject.activeInHierarchy == true) {
            cagePrompt.gameObject.SetActive(false);
        }

        if (cagePrompt.gameObject.activeInHierarchy == true) {
            buttonPrompt.gameObject.SetActive(false);
        }
    }

    public void StoreLevelProgress() {
        locked = false; //unlock level exit if needed

        //level completion data
        string name = SceneManager.GetActiveScene().name;
        float time = TimeTrialManager.instance.FinishTimeTrial();

        //store progress
        ProgressionManager.instance.SaveProgress(name, time);

        //finishes level if true
        if (autoFinish) {
            FinishLevel();
        }
    }

    public void FinishLevel() {
        //Debug.Log("Back to Level Select");
        if (!locked) {
            LoadManager.instance.TransitionToScene(nextSceneIndex);
        }
    }

    // disable button prompt
    void OnTriggerEnter(Collider other) {
        if (other.gameObject.tag == player.tag && !locked) {
            buttonPrompt.gameObject.SetActive(true);
        }
    }

    void OnTriggerStay(Collider other) {
        // If you're in Trigger and you have your cage
        if (other.gameObject.tag == player.tag) {
            //Debug.Log("can drop again: " + player.GetComponent<PlayerModel>().DropButtonEnabled);
            player.GetComponent<PlayerModel>().DropButtonEnabled = false;

            if (InputManager.InteractButton(false))
                FinishLevel();
        }
    }

    // enable button prompt
    void OnTriggerExit(Collider other) {
        if (other.gameObject.tag == player.tag) {
            //Debug.Log("can drop again: " + player.GetComponent<PlayerModel>().DropButtonEnabled);
            player.GetComponent<PlayerModel>().DropButtonEnabled = true;
            buttonPrompt.gameObject.SetActive(false);
        }
    }

    //used for cutscene activations
    public void Activate(float activationParameter) {
        StoreLevelProgress();
    }
}