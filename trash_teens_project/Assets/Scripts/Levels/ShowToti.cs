﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowToti : MonoBehaviour {
    [SerializeField] GameObject[] toti;

    void Start() {
        foreach (GameObject t in toti) {
            t.SetActive(false);
        }
    }

    void OnTriggerEnter(Collider other) {
        if (other.gameObject.tag == "Player" && other.gameObject.GetComponent<PlayerModel>().carryScript.IsCarrying) {
            foreach (GameObject t in toti) {
                t.SetActive(true);
                t.transform.SetParent(null);
            }

            Destroy(this.gameObject);
        }
    }
}
