﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeTrialManager : MonoBehaviour {
    //static instance can be called any time
    public static TimeTrialManager instance;

    float startTime; //time player begins to complete the level
    float completionTime; //time player took to finish the level

    public float CompletionTime {
        get { return completionTime; }
    }

    //avoids multiple instances running at same time
    void Awake() {
        if (instance != null && instance != this) {
            Destroy(this.gameObject);
        }
        else {
            instance = this;
        }
    }

    public void StartTimeTrial() {
        startTime = Time.time;
    }

    public float FinishTimeTrial() {
        completionTime = Time.time - startTime;
        return completionTime;
    }
}