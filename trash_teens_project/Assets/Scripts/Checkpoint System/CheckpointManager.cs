﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MSuits.Checkpoint {
    public enum CheckpointCondition { None, WithCage, ButtonActivated }

    public class CheckpointManager : MonoBehaviour {
        //static instance can be called any time
        public static CheckpointManager instance;

        //stores last CheckpointTrigger in order to avoid repeating checkpoints
        CheckpointTrigger lastTrigger;
        CheckpointCondition lastCondition;

        //singleton setup
        void Awake() {
            if (instance != null && instance != this) {
                Destroy(this.gameObject);
            }
            else {
                instance = this;
            }
        }

        //save checkpoint only if it is a new CheckpointTrigger
        public void SaveCheckpoint(CheckpointTrigger _checkpointTrigger) {
            if (lastTrigger != _checkpointTrigger || lastCondition != _checkpointTrigger.CurrentCondition) {
                if (lastTrigger != null) {
                    lastTrigger.ChangeSemaphor(false);
                    UIManager.instance.ActivateCheckpoint();
                }
                
                _checkpointTrigger.ChangeSemaphor(true);
                lastTrigger = _checkpointTrigger;
                lastCondition = _checkpointTrigger.CurrentCondition;       
            }
        }

        public void LoadCheckpoint() {
            if (lastTrigger != null) {
                lastTrigger.LoadCheckpoint(); //makes all CheckpointObjects return to last CheckpointTrigger state
            }
        }
    }
}