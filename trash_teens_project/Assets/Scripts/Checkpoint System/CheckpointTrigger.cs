﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MSuits.Checkpoint.Objects;

namespace MSuits.Checkpoint {
    [RequireComponent(typeof(BoxCollider))]
    public class CheckpointTrigger : MonoBehaviour {
        //tag from the object that triggers the checkpoint behaviour
        string playerLayer = "Player", cageLayer = "Carryable";

        //list of all CheckpointObjects attached to this CheckpointTrigger
        List<CheckpointObject> checkpointObjects;

        //stores current checkpoint condition
        CheckpointCondition currentCondition;
        public CheckpointCondition CurrentCondition { get { return currentCondition; } }

        //checkpoint visual cue
        Animator anim;

        public void LoadCheckpoint() {
            //lazy load CheckpointObjects
            if (checkpointObjects == null) {
                checkpointObjects = new List<CheckpointObject>();
                checkpointObjects.AddRange(GetComponents<CheckpointObject>());
            }

            //loads respective CheckpointObjects based on condition
            foreach (CheckpointObject checkpointObject in checkpointObjects) {
                checkpointObject.LoadCheckpoint(currentCondition);
            }
        }

        public void ChangeSemaphor(bool state) {
            //lazy load animator
            if (anim == null) {
                anim = GetComponent<Animator>();
            }
            anim.SetBool("Checkpoint", state);
        }

        void OnTriggerEnter(Collider other) {
            string layer = LayerMask.LayerToName(other.gameObject.layer);           

            if (layer == playerLayer) {
                CheckpointManager.instance.SaveCheckpoint(this); //makes CheckpointManager save checkpoint                              

                if (currentCondition != CheckpointCondition.WithCage) {
                    string childLayer;
                    foreach (Transform child in other.gameObject.transform) {
                        childLayer = LayerMask.LayerToName(child.gameObject.layer);

                        if (childLayer == cageLayer) {                            
                            currentCondition = CheckpointCondition.WithCage; //changes condition if Player holds Cage
                            break;
                        }
                    }
                }

                return;
            }

            if (layer == cageLayer) {
                currentCondition = CheckpointCondition.WithCage; //changes condition if Cage is thrown at CheckpointTrigger
            }
        }
    }
}