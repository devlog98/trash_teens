﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MSuits.Checkpoint.Objects {
    public abstract class CheckpointObject : MonoBehaviour {
        //responsible for loading specific variables from objects
        public abstract void LoadCheckpoint(CheckpointCondition _checkpointCondition);
    }
}