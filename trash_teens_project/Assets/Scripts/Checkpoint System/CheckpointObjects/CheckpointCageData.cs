﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MSuits.Checkpoint.Objects {
    [Serializable]
    public class CheckpointCageData {
        public CheckpointCondition checkpointCondition;
        public Transform transform;
    }
}