﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MSuits.Checkpoint.Objects {
    [RequireComponent(typeof(CheckpointTrigger))]
    public class CheckpointPlayer : CheckpointObject {
        [SerializeField] PlayerModel player; //reference to PlayerModel
        [SerializeField] List<CheckpointPlayerData> checkpoints; //list of possible checkpoint outcomes based on CheckpointConditions

        public override void LoadCheckpoint(CheckpointCondition _checkpointCondition) {
            CheckpointPlayerData checkpoint = checkpoints.Find(x => x.checkpointCondition == _checkpointCondition);

            if (checkpoint != null) {
                //player must drop cage
                player.Drop(player.carryDrop.transform.position);

                //reset PlayerModel data
                player.transform.position = checkpoint.transform != null ? checkpoint.transform.position : checkpoint.position;
                player.playerModel.eulerAngles = checkpoint.transform != null ? checkpoint.transform.eulerAngles : checkpoint.rotation;                
            }
        }
    }
}