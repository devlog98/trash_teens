﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MSuits.Checkpoint.Objects {
    [Serializable]
    public class CheckpointPlayerData {
        public CheckpointCondition checkpointCondition;
        public Transform transform;
        public Vector3 position;
        public Vector3 rotation;
    }
}