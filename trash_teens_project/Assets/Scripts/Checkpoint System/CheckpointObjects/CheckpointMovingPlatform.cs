﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MSuits.Checkpoint.Objects {
    [RequireComponent(typeof(CheckpointTrigger))]
    public class CheckpointMovingPlatform : CheckpointObject {
        [SerializeField] MovingPlatform platform; //reference to MovingPlatform
        [SerializeField] ButtonManager buttonManager; //reference to respective ButtonManager that activates MovingPlatform
        [SerializeField] List<CheckpointMovingPlatformData> checkpoints; //list of checkpoints and what data they change        

        public override void LoadCheckpoint(CheckpointCondition _checkpointCondition) {
            CheckpointMovingPlatformData checkpoint = checkpoints.Find(x => x.checkpointCondition == _checkpointCondition);

            if (checkpoint != null) {
                //reset MovingPlatform data
                platform.transform.position = checkpoint.transform.position;
                platform.ElapsedTime = checkpoint.elapsedTime;

                if (checkpoint.resetButtonManager) {
                    platform.Deactivate();

                    //reset ButtonManager data
                    buttonManager.Deactivate();
                }
            }           
        }
    }
}