﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MSuits.Checkpoint.Objects {
    [Serializable]
    public class CheckpointMovingPlatformData {
        public CheckpointCondition checkpointCondition;
        public Transform transform; //change platform placement via position
        public float elapsedTime; //change platform placement via animation progression
        public bool resetButtonManager;
    }
}