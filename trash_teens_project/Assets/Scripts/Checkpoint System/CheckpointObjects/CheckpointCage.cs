﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MSuits.Carryable;

namespace MSuits.Checkpoint.Objects {
    [RequireComponent(typeof(CheckpointTrigger))]
    public class CheckpointCage : CheckpointObject {
        [SerializeField] Cage cage; //reference to Cage object
        [SerializeField] List<CheckpointCageData> checkpoints; //list of possible checkpoint outcomes based on CheckpointConditions

        public override void LoadCheckpoint(CheckpointCondition _checkpointCondition) {
            CheckpointCageData checkpoint = checkpoints.Find(x => x.checkpointCondition == _checkpointCondition);

            if (checkpoint != null) {
                //reset Cage data
                cage.transform.position = checkpoint.transform.position;
                cage.transform.rotation = checkpoint.transform.rotation;
                cage.StopFallVelocity(); //avoids object accumulating speed when returning
            }
        }
    }
}