﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CageCheckpoint : MonoBehaviour {

    public Transform returnPoint;
    public GameObject player;
    bool isActivated;

    private void Start()
    {
        this.transform.position = CheckpointData.checkpointControl.cagePos;
    }

    // Update is called once per frame
    void Update () {
		
        if(transform.position.y <= -100)
        {
            player.GetComponent<PlayerModel>().SendMessage("PlayerDeath");
        }

	}

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "CageCheckpoint")
        {
            if(this.GetComponent<Rigidbody>().isKinematic)
            {
                returnPoint = other.transform.GetChild(0);
                other.GetComponent<ActivateCheckpoint>().Activate();
            }
        }
    }

    public void ResetPosition()
    {
        if (returnPoint != null)
        {
            GetComponent<Rigidbody>().velocity = Vector3.zero;
            GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
            transform.position = returnPoint.position;
        }
        else
        {
            GetComponent<Rigidbody>().velocity = Vector3.zero;
            GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
            transform.position = GetComponent<ResetCagePosition>().defaultOriginalPosition.position;
        }
    }
}
