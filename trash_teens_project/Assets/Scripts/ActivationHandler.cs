﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ButtonType { OneTime, Toggle, Hold }

public class ActivationHandler : MonoBehaviour, IActivatable {
    public bool isToggle;
    public bool isActive;
    [SerializeField] ButtonType buttonType;

    public MeshRenderer mesh;
    public Animator anim;

    [SerializeField] int frameInterval = 1;

    void Update() {
        if (Time.frameCount % frameInterval == 0) {
            if (mesh != null) {
                if (isActive) {
                    mesh.material.color = Color.green;
                }
                else {
                    mesh.material.color = Color.red;
                }
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (!isToggle && other.gameObject.tag != "Enemy" && other.gameObject.name != "GrabbingHitbox")
        {
            anim.SetTrigger("Pressed");
            isActive = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if(anim != null)
            anim.SetTrigger("Released");

        if (!isToggle && other.gameObject.tag != "Enemy" && other.gameObject.name != "GrabbingHitbox" && isActive == true)
        {
            isActive = false;
        }
    }

    private void OnTriggerEnter(Collider other) {

        if (anim != null)
            anim.SetTrigger("Pressed");

        if (isToggle && other.gameObject.tag != "Enemy" && other.gameObject.name != "GrabbingHitbox") {

            isActive = !isActive;
        }
    }

    //TO CHANGE IN THE FUTURE
    //private void OnTriggerEnter(Collider other) {
    //    if (other.gameObject.name != "GrabbingHitbox") {
    //        anim.SetTrigger("Pressed");

    //        switch (buttonType) {                
    //            case ButtonType.Toggle:
    //                isActive = !isActive;
    //                break;
    //            default:
    //                isActive = true;
    //                break;
    //        }
    //    }
    //}

    //private void OnTriggerExit(Collider other) {
    //    if (other.gameObject.name != "GrabbingHitbox") {    
    //        switch (buttonType) {
    //            case ButtonType.Toggle:
    //                anim.SetTrigger("Released");
    //                break;
    //            case ButtonType.Hold:
    //                anim.SetTrigger("Released");
    //                isActive = false;
    //                break;
    //        }
    //    }
    //}

    public void Activate(float activationParameter) {
        throw new System.NotImplementedException();
    }
}